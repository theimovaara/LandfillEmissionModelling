"""
## Eval_SL_WM_DREAM01_longterm_i.py
Script to call Emission_LF.likelihood_Emission_LF() with parameter sets inferred with pyDREAM.

This script extrapolates in to the future using time data defined in Initialize_WM_DREAM_LT.

Script only runs code to store results in memory. Script can be loaded as a module by a plotting script.

Created on Sat Oct 20 2018

@author: T.J. Heimovaara
Revised: Jul 3 2024

"""
# %%
import numpy as np
import pandas as pd
import scipy.stats as scpstats
import matplotlib.pyplot as plt
import seaborn as sns
import Initialize_WM_DREAM_LT as iBB
from context import mymod_v4 as mymod

import pickle as pkl
from pydream.convergence import Gelman_Rubin

from context import iniParRange_2TTi as ipr
# from context import iniParRange_2TTb_dirac as ipr

# %%
loc_name = 'Wieringermeer VP06'
plt_ext = '_WM01.pdf'

ipr.results_dir = './Results_2TTi/'
d_max = 40000  # 2TTc

# ipr.results_dir = './Results_2TTc_1_unlim/'
# d_max = 70000  # 2TTc

# ipr.results_dir = './Results_2TTc_lim_local/'
# d_max = 20000  # 2TTc

# ipr.results_dir = './Results_2TTc_lim/'
# d_max = 60000  # 2TTc

# ipr.results_dir = './Results_2TTc_2_unlim/'
# d_max = 40000  # 2TTc

# ipr.results_dir = './Results_2TTc_2_lim_05/'
# d_max = 20000  # 2TTc

nchains = 3
#d_max = 710000  #2TTa
d_iter = 10000

# from context import iniParRange_noBias_3chains as ipr
# nchains = 3
# d_iter = 10000


# set seaborn graphics style
sns.set()

par_df = ipr.par_df

statpar_df = ipr.statpar_df
# statpar_df.loc[0,'wBqMin'] = iBB.wBqMin
# statpar_df.loc[0,'wBcMin'] = iBB.wBcMin

simType = 'ForwardSim'  # or 'est' for optimization


def likelihood(par):
    # Run Forward model using parameters in par
    logp, emOut, lFPar, emLF, like_stats_qDr2, like_stats_cDr, likPar_qDr, likPar_cDr \
        = mymod.likelihood_Emission_LF(par, simType, iBB.tdseries, iBB.lF,
                                       iBB.meteo_data,
                                       iBB.meas_data_flow, iBB.lab_data,
                                       par_df, statpar_df, flow_only=False)

    return logp, emOut, lFPar, emLF, like_stats_qDr2, like_stats_cDr, likPar_qDr, likPar_cDr


npar = par_df.shape[1]


parfn = ipr.results_dir + 'WM01_SL_'
logpsfn = ipr.results_dir + 'WM01_SL_logps_'

sampled_params = np.zeros([nchains, d_max, npar])
log_ps = np.zeros([nchains, d_max])

# Loop through all files
for chain in np.arange(nchains):
    for nsamp in np.arange(d_iter, d_max+d_iter, d_iter):
        fn = parfn + str(chain) + '_' + str(nsamp) + '.npy'
        fnlogps = logpsfn + str(chain) + '_' + str(nsamp) + '.npy'
        sampled_params[chain][nsamp-d_iter:nsamp] = np.load(fn)
        log_ps[chain][nsamp-d_iter:nsamp] = np.load(fnlogps).squeeze()
        starts = [sampled_params[chain][-1, :] for chain in range(nchains)]

maxidx = np.unravel_index(log_ps.argmax(), log_ps.shape)
par = sampled_params[maxidx]
GR = Gelman_Rubin(sampled_params)

# save starts for possible restart of dream optimization
# save starts to pickle...
fo = open('starts01.pkl', 'wb')
pkl.dump(starts, fo)
fo.close()

# %%
# process data to generate table of statistics
nsamples = len(sampled_params[0])
nchains = len(sampled_params)
nburnin = nsamples//2

# if nsamples > 20000:
#     nburnin = 10000
# else:
#     nburnin = nsamples//2

allchains = np.append(sampled_params, log_ps.reshape(
    nchains, nsamples, 1), axis=2)
nms = ipr.par_df.columns
nms = nms.append(pd.Index(['log_ps']))

# samp_range = np.arange(150000,d_max)
# lsamp = len(samp_range)
# samp_pardf = pd.DataFrame(allchains[:, samp_range, :].reshape(lsamp*nchains, npar+1),
#                           columns=nms)

samp_pardf = pd.DataFrame(allchains[:, -nburnin:, :].reshape(nburnin*nchains, npar+1),
                          columns=nms)

# %%
sorteddf = samp_pardf.sort_values(by='log_ps', ascending=False)

quants = scpstats.mstats.mquantiles(
    sorteddf['log_ps'].values,prob=[1,0.95,0.5,0.05])

def find_nearest_index(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

iidx = np.zeros(len(quants))
for ii in range(len(quants)):
    iidx[ii] = find_nearest_index(sorteddf['log_ps'].values,
        quants[ii])



# fo = open('starts_Gaussian.pkl','rb')
# starts = pkl.load(fo)
# #fo.close()
# newstarts = []
# for ii in np.arange(5):
#     pars = starts[ii]
#     for jj in np.arange(18,30):
#         pars = np.append(pars,iBB.statpar_df[iBB.par_df.columns[jj]].values)
#     #add previous logp to pars
#     #pars = np.append(pars,starts[ii][17])
#     newstarts.append(pars)


#par = iBB.par_df.iloc[2].values
#par_set = newstarts
parconverged = samp_pardf.iloc[-nsamp:]
#par_set = sorteddf.iloc[iidx,:npar+1].values

par_tmp = pd.DataFrame(allchains[:, -1, :].reshape(nchains, npar+1), columns=nms)
par_tmp = pd.concat([par_tmp, sorteddf.iloc[:]])
#par_setdf = par_tmp.iloc[:4]

par_setdf = sorteddf.iloc[iidx]

par_set_stats = sorteddf.describe()
par_set_stats = pd.concat ([par_set_stats.T, par_setdf.T], axis = 1)
par_set_stats.to_excel('WM01_parset_i.xlsx')

# save the optimal, 95% and 5% for reinitializing the bayesian inference
new_starts = []
for ii in [0,1,3]:
    new_starts.append(par_setdf.iloc[ii][:-1].values)

fo = open('optim_starts01.pkl', 'wb')
pkl.dump(new_starts, fo)
fo.close()    

    
logp_tot = []
cumqDr_tot = []
cDr_tot = []
qDr_tot = []
qInf_tot = []
qev_tot = []

thCL_tot = []
mCL_tot = []
cInf_CL = []
mbalerr_CL = []
qrunoff_CL = []

vW_tot = []
mW_tot = []
totF_sim = []
lFPar_tot = []
drainLev_tot = []
drainLev_sim = []
drainOutF_tot = []
emLF_tot = []
bF_tot = []

like_stats_qDr2_tot = []
like_stats_cDr_tot = []

likPar_qDr_tot = []
likPar_cDr_tot = []


# %%
for ii in np.arange(len(par_setdf)):

    print('Calling Landfill forward model #', ii)
    par_set = par_setdf.iloc[ii].values[:-1]
    logp, emOut, lFPar, emLF, like_stats_qDr2, like_stats_cDr, likPar_qDr, likPar_cDr \
        = likelihood(par_set)

    cumqDr, cDr, qDr, qInf, qev, thCL, mCL, cInfCL, mbalerrCL, vW, mW \
        = emOut

    logp_tot.append(logp)
    cumqDr_tot.append(cumqDr)
    cDr_tot.append(cDr)
    qDr_tot.append(qDr)
    qInf_tot.append(qInf)
    qev_tot.append(qev)

    thCL_tot.append(thCL)
    mCL_tot.append(mCL)
    cInf_CL.append(cInfCL)
    mbalerr_CL.append(mbalerrCL)
    # qrunoff_CL.append(qrunoffCL)
    vW_tot.append(vW)
    mW_tot.append(mW)
    lFPar_tot.append(lFPar)
    emLF_tot.append(emLF)
    vWtmp = vW_tot[ii]
    bF_tot.append(emLF.BaseFlow(vWtmp[:, -1]))
    like_stats_qDr2_tot.append(like_stats_qDr2)
    like_stats_cDr_tot.append(like_stats_cDr)

    likPar_qDr_tot.append(likPar_qDr)
    likPar_cDr_tot.append(likPar_cDr)

    totF_sim.append(cumqDr_tot[ii][iBB.tmeas_ind] -
                    cumqDr_tot[ii][iBB.tmeas_ind[0]])


# %%
likPar_qDr_df = pd.DataFrame(likPar_qDr_tot)
likPar_cDr_df = pd.DataFrame(likPar_cDr_tot)

like_stats_qDr2_df = pd.DataFrame(like_stats_qDr2_tot)
like_stats_cDr_df = pd.DataFrame(like_stats_cDr_tot)

par_set_stats = sorteddf.describe()
par_set_stats = pd.concat([par_set_stats.T, par_setdf.T], axis=1)
nms = par_set_stats.columns

logL_qDr = like_stats_qDr2_df.logL.values
logL_cum = like_stats_qDr2_df.logL_cum.values
logL_cDr = like_stats_cDr_df.logL.values

logL_df = pd.DataFrame()
logL_df['logL_qDr'] = pd.Series(data=logL_qDr, index=nms[-4:])
logL_df['logL_cum'] = pd.Series(data=logL_cum, index=nms[-4:])
logL_df['logL_cDr'] = pd.Series(data=logL_cDr, index=nms[-4:])
logL_df['logL_tot'] = pd.Series(data=logp_tot, index=nms[-4:])

pd.concat([par_set_stats, logL_df.T])
# par_set_stats.to_excel('BB11N_parset_d.xlsx')

# Dream calculates log_ps as:
# log_ps[iteration] = log_like + log_prior
# we do not add this log_prior so we need add this to the log_ps


delta_log = logL_df.logL_tot - par_setdf.log_ps
# this are the same values for all dataset
delta_ps = delta_log.iloc[0]
sorteddf.log_ps = sorteddf.log_ps + delta_ps
allchains[:,:,npar] = allchains[:,:,npar] + delta_ps 
log_ps = log_ps + delta_ps

# fig1 = plt.figure(1)
# plt.plot(iBB.meas_data_flow.index, iBB.meas_data_flow['totF']
#         - iBB.meas_data_flow['totF'].iloc[iBB.tdseries.tcalmeas_ind[0]], 'o')
# for ii in range(len(par_setdf)):
#     plt.plot(iBB.tmeas, totF_sim[ii]-totF_sim[ii][iBB.tdseries.tcalmeas_ind[0]])
# plt.xlabel('year')
# plt.ylabel(r'cumulative leachate [$m^3$]')

fig_path = '../figures_i/'