"""
## Initialize_BB11N_DREAM01.py

Initialization for landfill cell Braambergen 11N
This version initializes the time data for parameter inference with measured data sets.
This script sets the site specific variables and then calls the database library
functions to import the site specific data.

Created on Sat Oct 20 2018
Modules for extracting data from the Chronos database

@author: T.J. Heimovaara
revision 20240703
"""

 # %%
import numpy as np
import scipy as sp
import scipy.stats as stats
import pandas as pd
import matplotlib.pyplot as plt
from context import dbl
# %matplotlib qt5
#
# Meteorological data will be obtained from two sources:
# 1: a close by weather station (for WM Berkhout, for BB: Lelystad)
#    we will use the evapotranspiration data obtained from the weather station...
# 2: rainfall from the 1km gridded radar corrected interpolated rainfall data obtained
#    from climate4impact...

# surface areas of Kragge compartment 3 and 4

# In[0]: Import data from KNMI
weather_station = '269'  # Lelystad
t_range = ['20030101', '20241031']

pklfile = './DataFiles/meteoLS.gz'
#path = './MeteoData/WM_Rain_2008-2019.bz2'

#inpfile = 'etmgeg_269.txt'
# Read data from close by meteo station
#meteo_data_stat = dbl.download_meteoKNMI_etmgeg (t_range, weather_station, pklfile, inpfile)

meteo_data_stat = dbl.download_meteoKNMI(t_range, weather_station, pklfile)
meteo_data_stat = meteo_data_stat.rename(columns={'rain': 'rain_station'})

# Read data from gridded radar corrected interpolated rainfall data
#ain_radar = pd.read_pickle(fpath,compression='infer')
# transform rain values from kg/m2 (mm) to m water column
#ain_radar['rain'] = rain_radar['rain']/1e3
# Merge the station data and the interpolated rain data in to a single dataframe
meteo_data = meteo_data_stat
# meteo_data is top boundary condition. We run the model from 2003 onward
meteo_data = meteo_data[slice(t_range[0], t_range[1])]

# Define simulation time range (trange)
trange = pd.date_range(start=t_range[0], end=t_range[1], freq='D')

# eteo_data.rain.loc[meteo_data['rain'].isnull()] = \
#   meteo_data.rain_station.loc[meteo_data['rain'].isnull()]

# %%
# Download flow and level data from CHRONOS
pump_code = 'PP-11N'

# Obtain landfill specific properties
cellIdx = 0  # 11N = 0, 11Z = 1, 12 = 2
lF = dbl.wastebodyPropertiesBB(cellIdx)  # m2


pklfile = './DataFiles/flowdata_PP-11N.gz'
df_inline = dbl.download_flow_level(pump_code, pklfile)

# We create a pivot table based on column cname (component names)
# inline_par = pd.pivot_table(df_inline, values='rawvalue', index=['datetime'],
#                      columns=['cname'], aggfunc=np.sum)

# %%
totF0 = pd.pivot_table(df_inline, values='value', index=['datetime'],
                       columns=['cname'], aggfunc='mean')

# totF0 = pd.pivot_table(df_inline, values='rawvalue', index=['datetime'],
#                        columns=['cname'], aggfunc='mean')


# totF = dbl.remove_outliers_inline(totF0)
totF = dbl.remove_outliers_inline(totF0,['20120601', t_range[1]])/lF.surfArea

# totF = dbl.remove_outliers_inlineBB(totF0, ['20120601', t_range[1]])/lF.surfArea

# as the model allows for leachate recirculation it expects a totIniflF dataset
# For a situation where no leachate is recirculated, we set the totIniflF to zero

totF0['totInfilF'] = 0

levelD = totF0['level']
infilF = totF0['totInfilF']

# %%
#sensData = dbl.download_sens_data_Kragge (pump_code, tmeas, pklfile)

# We create a pivot table based on column cname (component names)
# inline_par = pd.pivot_table(df_inline, values='rawvalue', index=['datetime'],
#                      columns=['cname'], aggfunc=np.sum)
#totF = sensData['totalFlow']
# levelD = sensData['levelD']from context import ipr
#infilF = sensData['totInfilF']

# Download laboratory data for pump pit
pklfile = './DataFiles/labdata_PP-11N.gz'

df_lab = dbl.download_lab(pump_code, pklfile)

# We create a pivot table based on column cname (component names)
# lab_data = pd.pivot_table(df_lab, values='value', index=['date'],
#                           columns=['cname'], aggfunc='sum')


# For now we will only use the chloride data in the dataset
df_lab['datetime'] = pd.to_datetime(df_lab['date'])
sel_idx = df_lab['cname']=='Chloride'

lab_data = df_lab.loc[sel_idx,['datetime','value']]


lab_data = lab_data.set_index('datetime')
lab_data = lab_data.rename(columns={'value': 'Cl'})

# Select measurements, should fall within trange.
# tmeas contains times where measurements are available!
# can have multiple tmeas vectors for different types of measurements
# totF contains measured data from mid 2012. We choose to start on the 2012-07-01
# Because the outflow is influenced by operator decisions we choose to select weekly
# cumulative totals...
measFreq = 7
tmeas = pd.date_range(start='20120614', end=t_range[1], freq='7D')
finter = sp.interpolate.interp1d(totF.index.view(np.int64), totF.values)
totF_val = finter(tmeas.view(np.int64))
totF2 = pd.DataFrame(data=totF_val, index=tmeas)
totF2 = totF2-totF2.iloc[0]
meas_data_flow = totF2.rename(columns={0: 'totF'})

# Define calibration time range. This will be used by DREAM to compare
# simulated values with calibration set...
# Data set to be matched by modifying parameters...
tcalib = pd.date_range(start='20140101', end='20201231', freq='D')

# In order to facilitate quick and easy comparison of simulation with data
# we need to define the overlapping indices:
# tmeas_ind: trange[tmeas_ind] = tmeas
# tcalib_ind: trange[tcalib_ind]=tcalib
# tcalmeas_ind, tmeascal_ind: tmeas[tcalmeas_ind]=tcalib[tmeascal_ind]


xy, ind1, tmeas_ind = np.intersect1d(tmeas,  trange,
                                     return_indices=True)
xy, ind1, tcalib_ind = np.intersect1d(tcalib, trange,
                                      return_indices=True)
xy, tmeascal_ind, tcalmeas_ind = np.intersect1d(tcalib, tmeas,
                                                return_indices=True)

xy, tlabrange_ind, trangelab_ind = np.intersect1d(lab_data.index,  trange,
                                                return_indices=True)

xy, tlabmeas_ind, tmeaslab_ind = np.intersect1d(lab_data.index,  tmeas,
                                                return_indices=True)

xy, tmeascal_lab_ind, tcalmeas_lab_ind = np.intersect1d(tcalib, lab_data.index,
                                                        return_indices=True)


tdata = {'trange': trange,
         'tmeas': tmeas,
         'tcalib': tcalib,
         'tmeas_ind': tmeas_ind,
         'tcalib_ind': tcalib_ind,
         'tcalmeas_ind': tcalmeas_ind,
         'tmeascal_ind': tmeascal_ind,
         'tlabmeas_ind': tlabmeas_ind,
         'tmeaslab_ind': tmeaslab_ind,
         'tmeascal_lab_ind': tmeascal_lab_ind,
         'tcalmeas_lab_ind': tcalmeas_lab_ind,
         'tlabrange_ind': tlabrange_ind,
         'trangelab_ind': trangelab_ind}

tdseries = pd.Series(tdata)


# Obtain landfill specific properties
cellIdx = 0  # 11N = 0, 11Z = 1, 12 = 2
lF = dbl.wastebodyPropertiesBB(cellIdx)  # m2

# Run model wil optimal parameter set...

# Prepare DREAM model...
# Model parameters which are required to calculate fluxes etc. (often need to
# optimized).

#par_df, statpar_df = ipr.set_ini_parRange()

# %%
