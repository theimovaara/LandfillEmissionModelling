"""
Python script which runs the model for parameter sets by loading Eval_SL_BB11N_DREAM02_longterm_i.
Then script proceeds to plot output for a wide range of results. 
Script generates all plots used in paper.

Created on Fri Oct 23 13:59:18 2020

@author: theimovaara

Revised: Jul 3 2024
"""
# %%
# Plot measurements in plots
import matplotlib.pyplot as plt
import seaborn as sns
from Eval_SL_BB11N_DREAM02_longterm_i import *
# In[0] 2 run only plotting routines...
import scipy.stats as scpstats
%matplotlib qt5

# In[1] Initialize legends and other variables

#clist = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
clist = sns.color_palette('tab10', n_colors=10)
sns.set_palette(clist)
sns.set_style('whitegrid')
sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 1})
dpi_plt = 300
leg_text0 = ['0', '1', '2', '3', '4']

leg_text1 = ['meas']
logps = logL_df.logL_tot.values
[leg_text1.append(str(int(np.rint(logps[ii])))) for ii in range(len(logps))]

leg_text2 = leg_text1[1:]

leg_text1_qDr = ['meas']
logps = logL_df.logL_qDr.values
[leg_text1_qDr.append(str(int(np.rint(logps[ii])))) for ii in range(len(logps))]
leg_text2_qDr = leg_text1_qDr[1:]

leg_text1_cum = ['meas']
logps = logL_df.logL_cum.values
[leg_text1_cum.append(str(int(np.rint(logps[ii])))) for ii in range(len(logps))]
leg_text2_cum = leg_text1_cum[1:]

leg_text1_cDr = ['meas']
logps = logL_df.logL_cDr.values
[leg_text1_cDr.append(str((np.rint(logps[ii])))) for ii in range(len(logps))]
leg_text2_cDr = leg_text1_cDr[1:]

n2006 = np.where(iBB.trange == '2008-01-01')[0][0]
plt.close('all')

# In[2] plot first 10 plots
fig0 = plt.figure(0)
plt.plot(log_ps.T, '.')
plt.ylim(0, 3400)
plt.autoscale(enable=True, axis='x', tight=True)
plt.title('log_ps')
plt.legend(leg_text0)


fig1 = plt.figure(1, figsize=(15,10))
plt.plot(iBB.meas_data_flow.index, iBB.meas_data_flow['totF']
         - iBB.meas_data_flow['totF'].iloc[iBB.tdseries.tcalmeas_ind[0]], 'o', color=clist[9])
for ii in range(len(par_setdf)):
    plt.plot(iBB.tmeas, totF_sim[ii]-totF_sim[ii]
             [iBB.tdseries.tcalmeas_ind[0]], color=clist[ii])
plt.autoscale(enable=True, axis='x', tight=True)
plt.xlabel('year')
plt.ylabel(r'cumulative leachate [$m$]')
plt.title(loc_name)
plt.legend(leg_text1)


fig2 = plt.figure(2, figsize=(15,10))
plt.clf()
plt.plot(iBB.tmeas, iBB.meas_data_flow.diff().values /
         iBB.measFreq, '.', color=clist[9])
for ii in range(len(par_setdf)):
    plt.plot(iBB.tmeas[1:], np.diff(totF_sim[ii])/iBB.measFreq, color=clist[ii])
plt.autoscale(enable=True, axis='x', tight=True)
plt.xlabel('year')
plt.ylabel(r'leachate pump rate [$m/day]$')
plt.title(loc_name)
plt.legend(leg_text1)

# %%
fig3 = plt.figure(3, figsize=(15,10))
plt.plot(iBB.lab_data.index, iBB.lab_data['Cl'], '.', color=clist[9])
for ii in range(len(par_setdf)):
    plt.plot(iBB.tmeas[0:], cDr_tot[ii][iBB.tmeas_ind], 
        color=clist[ii])
plt.xlim(iBB.tmeas[0],iBB.tmeas[-1])
#plt.autoscale(enable=True, axis='x', tight=True)
plt.xlabel('year')
plt.ylabel(r'Chloride [mg/L]')
plt.title(loc_name)
plt.legend(leg_text1)

# %%
fig4 = plt.figure(4, figsize=(15,10))
plt.clf()
for ii in range(len(par_setdf)):
    totF = (cumqDr_tot[ii][n2006:] - cumqDr_tot[ii][iBB.tmeas_ind[0]])
    plt.plot(iBB.trange[n2006:], np.diff(totF), color=clist[ii])
plt.autoscale(enable=True, axis='x', tight=True)
plt.title(loc_name + ': long term, water flow')
plt.xlabel('year')
plt.ylabel('leachate pumprate [$m^3/d$]')
plt.legend(leg_text2)

fig5 = plt.figure(5, figsize=(15,10))
for ii in range(len(par_setdf)):
    plt.plot(iBB.trange[n2006:], cDr_tot[ii][n2006:], color=clist[ii])
plt.autoscale(enable=True, axis='x', tight=True)
plt.title(loc_name + ': long term, outflow concentration')
plt.xlabel('year')
plt.ylabel(r'Chloride [mg/L]')
plt.legend(leg_text2)

fig6, ax6 = plt.subplots(3,1, sharex=True, figsize=(17,13))
[ax6[0].plot(iBB.trange[n2006:], mW_tot[ii][n2006:, -1]/vW_tot[ii][n2006:, -1], 
             color=clist[ii]) for ii in range(len(par_setdf))]
[ax6[1].plot(iBB.trange[n2006:], mW_tot[ii][n2006:, -1], 
             color=clist[ii]) for ii in range(len(par_setdf))]
[ax6[2].plot(iBB.trange[n2006:], 
          vW_tot[ii][n2006:, -1],color=clist[ii]) for ii in range(len(par_setdf))]
ax6[0].autoscale(enable=True, axis='x', tight=True)
ax6[0].set_title(loc_name + ': bulk')
ax6[0].set_ylabel(r'$C_{bulk}$ [mg/L]')
ax6[1].set_ylabel(r'$M_{bulk}$ [g/m2]')
ax6[2].set_ylabel(r'$S_{bulk}$ [m]')
ax6[2].set_xlabel('year')

ax6[0].legend(leg_text2)
fig6.savefig(fig_path + 'bulk_concentration' + plt_ext,dpi = dpi_plt)

fig7 = plt.figure(7, figsize=(15,10))
[plt.plot(iBB.trange[n2006:], mW_tot[ii][n2006:, -1], color=clist[ii])
 for ii in range(len(par_setdf))]
plt.autoscale(enable=True, axis='x', tight=True)
plt.title(loc_name +': bulk mass')
plt.xlabel('year')
plt.ylabel(r'$M_{{Cl}^-}$ [g/m2]')
plt.legend(leg_text2)
#fig7.savefig(fig_path + 'bulk_mass' + plt_ext,dpi = dpi_plt)

fig8 = plt.figure(8, figsize=(15,10))
plt.clf()
[plt.plot(iBB.trange[n2006:], 
          mW_tot[ii][n2006:, -1] / np.sum(mW_tot[ii][n2006:, :], axis=1),
          color=clist[ii]) for ii in range(len(par_setdf))]
plt.autoscale(enable=True, axis='x', tight=True)
plt.title(loc_name + ': bulk / total mass')
plt.xlabel('year')
plt.ylabel(r'$M_{{Cl}^-} [g/m^2]$')
plt.legend(leg_text2)
#fig8.savefig(fig_path + 'bulk_mass_fraction' + plt_ext,dpi = dpi_plt)

fig9 = plt.figure(9, figsize=(15,10))
plt.clf()
[plt.plot(iBB.trange[n2006:], vW_tot[ii][n2006:, -1], color=clist[ii])
 for ii in range(len(par_setdf))]
plt.autoscale(enable=True, axis='x', tight=True)
plt.title(loc_name + ': bulk storage')
plt.xlabel('year')
plt.ylabel(r'$S_{bulk}$ [m]')
plt.legend(leg_text2)
fig9.savefig(fig_path + 'bulk_storage' + plt_ext,dpi = dpi_plt)

# %%
fig10 = plt.figure(10, figsize=(17,13))
for ii in range(len(par_setdf)):
    vWtmp = np.arange(0, 9, 0.01)
    bFlow = emLF_tot[ii].BaseFlow(vWtmp)
    plt.plot(vWtmp, (bFlow), color=clist[ii])
plt.autoscale(enable=True, axis='x', tight=True)
plt.title(loc_name + ': base flow function')
plt.xlabel(r'$S_{bulk}$ [m]')
plt.ylabel(r'$q_{b_F}$ [m/d]')
plt.legend(leg_text2)
fig10.savefig(fig_path + 'base_flow_function' + plt_ext,dpi = dpi_plt)


# %%
#best_par = pd.Series(par, index=par_df.columns)
#allpardf = pd.concat(par_setdf.append(best_par, ignore_index=True)
allpardf = par_setdf
print(allpardf.T)

# In[3] for manual selection to check convergence
plot_chains = False
if plot_chains:
    plt.close('all')
    jj = np.arange(nchains)
    for ii in np.arange(npar):
        plt.figure()
        plt.clf()
        plt.plot(sampled_params[:, :, ii].T, '.')
        plt.title(samp_pardf.columns[ii])

    # plt.close(fig4)


plot_dists = False
if plot_dists:
    plt.close('all')
    #colors = sns.color_palette(n_colors=npar+1)
    nsamp2 = nburnin

    #figdist, axdist = plt.subplots(1, 1, figsize=(5, 5))
    for dim in range(npar+1):
        fig = plt.figure(figsize = (15,10))
        # for jj in range(nchains):
        #     sns.histplot(data=allchains[jj, -nsamp2:, dim],
        #                  stat='density', bins=15, kde=False,
        #                  cumulative=True,element='step',fill=False,
        #                  color=clist[jj])
        aap = sns.histplot(data=allchains[:, -nsamp2:, dim].reshape(3*nsamp2),
                     stat='density', bins=15, kde=False,
                     cumulative=True,element='step',fill=False,
                     color='k', linewidth=2)
        #sns.displot(samp_par_reshaped[:,dim], color=colors[dim],stat='probability',bins=25)
        plt.autoscale(enable=True, axis='x', tight=True)
        aap.set_xlabel(samp_pardf.columns[dim], fontsize=25)
        aap.set_ylabel('Density',fontsize=25)
        aap.yaxis.set_tick_params(labelsize=20)
        aap.xaxis.set_tick_params(labelsize=20)
        

        fig.savefig('../dist_figs_i/' + samp_pardf.columns[dim] + plt_ext)

    plt.close('all')
    sixidx = [[0,12,14],
              [13,7, 9]]
    nsamp2 = nburnin
    fig6,ax6 = plt.subplots(3,2, figsize=(20,30))
    for col in range(2):
        for row in range(3):
            # calculate cumulative plot for 


            dim = sixidx[col][row]
            sns.histplot(data=allchains[:, -nsamp2:, dim].reshape(3*nsamp2),
                     stat='density', bins=15, kde=False,
                     cumulative=True,element='step',fill=False,
                     color='k', linewidth=2,
                     ax=ax6[row,col])

            # plot prior range
            min_val = ipr.par_df.loc[0][dim]
            max_val = ipr.par_df.loc[1][dim]
            range_val = max_val - min_val
            x = np.arange(min_val, max_val,0.01)
            cdf_vals = scpstats.uniform.cdf(x,min_val, range_val)
            ax6[row,col].plot(x,cdf_vals, 'b--',linewidth=2)
            
            plt.autoscale(enable=True, axis='x', tight=True)
            ax6[row,col].set_xlabel('$'+ipr.par_df.loc[3][dim] + '$', fontsize=25)
            ax6[row,col].set_ylabel('Density',fontsize=25)
            ax6[row,col].yaxis.set_tick_params(labelsize=20)
            ax6[row,col].xaxis.set_tick_params(labelsize=20)
    
    # fig6.subplots_adjust(hspace=0.5)
    fig6.savefig('../dist_figs_i/sixplots' + plt_ext)
    

    plt.close('all')
    ndims = len(allchains[0][0])
    nsamp2 = nburnin
    lpars = allchains[:, -nsamp2:, :].reshape(nchains*nsamp2, ndims).T
    df_par = pd.DataFrame(lpars).T
    df_par.columns = par_setdf.columns
    #fig = sns.pairplot(df_par,markers='.')
    # fig.savefig('WM01_SL_PairPlot')
    pearson_ccoef = df_par.corr(method = 'pearson')
    kendall_ccoef = df_par.corr(method = 'kendall')
    spearman_ccoef = df_par.corr(method = 'spearman')
    
    ccoef = pearson_ccoef
    # hicor = np.where((np.abs(ccoef) < 0.5))
    hicor = np.where((np.abs(ccoef) > 0.75) & (np.abs(ccoef) < 0.9999))
    for ii in range(len(hicor[0])):
        plt.figure(ii)
        plt.plot(allchains[:,-nsamp2:,hicor[0][ii]].T,
                 allchains[:,-nsamp2:,hicor[1][ii]].T, '.')
        # plt.plot(lpars[hicor[0][ii]].T,
        #          lpars[hicor[1][ii]].T, '.')
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.xlabel(df_par.columns[hicor[0][ii]])
        plt.ylabel(df_par.columns[hicor[1][ii]])
    
    pearson_ccoef.to_excel('pearson_coef.xlsx')
    kendall_ccoef.to_excel('kendall_coef.xlsx')
    spearman_ccoef.to_excel('spearman_coef.xlsx')


# In[4] plots for interpretation of results
# Interprete the results with respect to the baseline measurements
fDM = 0.773  # fraction of dry matter for WM
# dry weight per m2 landfill [kg/m2]
dryWeightpm2 = iBB.lF.wetWeight * fDM / iBB.lF.surfArea

# dry density landfill
dryDensity = dryWeightpm2 / iBB.lF.height

print('dryWeightpm2 =', dryWeightpm2)
print('dryDensity = ', dryDensity)

# plot development in chloride mass per kg dry weight
fig11 = plt.figure(11, figsize=(17,13))
plt.clf()
[plt.plot(iBB.trange[n2006:], np.sum(mW_tot[ii][n2006:, :], axis=1) /
          dryWeightpm2, color=clist[ii]) for ii in range(len(par_setdf))]
plt.title(loc_name + ': total mass')
plt.autoscale(enable=True, axis='x', tight=True)
plt.xlabel('year')
plt.ylabel(r'$M_{{Cl}^-} [g/kg]$')
plt.legend(leg_text2)
fig11.savefig(fig_path + 'bulk_mass_per_kg' + plt_ext,dpi = dpi_plt)


# %%
# Error analaysis following Schoups and Vrugt 2010:
# Error analaysis following Schoups and Vrugt 2010:
fig12, ax12 = plt.subplots(1, 3, figsize=(20,10))
ndims = len(par_set)
colors = sns.color_palette(n_colors=ndims)
statsflow = np.zeros([len(par_set), 2])

#ObsqDr = iBB.meas_data_flow.loc[iBB.tdseries.tmeas[iBB.tdseries.tcalmeas_ind]].diff()

for ii in range(len(par_setdf)):
    # ObsqDr = qDr_tot[ii][ifig18 = plt.figure(18)
    #ax12[0].plot(ObsqDr[1:],likqDr.iloc[ii]['errDist'].squeeze(), '.')
    ax12[0].plot(like_stats_qDr2_df.iloc[ii]['ExpY'].squeeze(),
                 like_stats_qDr2_df.iloc[ii]['errDist'].squeeze(), '.',
                 color=clist[ii])

    sns.histplot(like_stats_qDr2_df.iloc[ii]['errDist'].squeeze(),
                 ax=ax12[1], stat='density', kde=False, color=clist[ii],
                 bins=75)
    ax12[1].plot(like_stats_qDr2_df.iloc[ii]['errDist'].squeeze(),
                 like_stats_qDr2_df.iloc[ii]['errDens'].squeeze(), '*',
                 color=clist[ii])
    #sns.kdeplot(likcDr.iloc[ii]['errDens'].squeeze(),ax=ax13[1], color=colors[ii])
    #sns.kdeplot(likqDr.iloc[ii]['errDens'].squeeze(),ax=ax12[1], color=clist[ii])

    pd.plotting.autocorrelation_plot(like_stats_qDr2_df.iloc[ii]['errDist'].squeeze(),
                                     ax=ax12[2], color=clist[ii])
    statsflow[ii, 0] = like_stats_qDr2_df.iloc[ii]['errDist'].squeeze().mean()
    statsflow[ii, 1] = like_stats_qDr2_df.iloc[ii]['errDist'].squeeze().std()

ax12[0].set_xlabel('flow')
ax12[0].set_ylabel('residuals')
ax12[1].set_xlabel('residuals')
ax12[1].set_title(loc_name + r': leachate pump rate [$m/day$]')
ax12[0].legend(leg_text2,bbox_to_anchor = (0.7, 1))
plt.tight_layout()
fig12.savefig(fig_path + 'errors_flow' + plt_ext,dpi = dpi_plt)

#print(statsflow)

# %%
fig13, ax13 = plt.subplots(1, 3, figsize=(20,10))
stats_c = np.zeros([len(par_set), 2])

for ii in range(len(par_setdf)):
    #ObscDr = cDr_tot[ii][iBB.tdseries.tcalib_ind[iBB.tdseries.tmeascal_lab_ind]]
    ax13[0].plot(like_stats_cDr_df.iloc[ii]['ExpY'].squeeze(),
                 like_stats_cDr_df.iloc[ii]['errDist'].squeeze(), '.',
                 color=clist[ii])
    #ax13[0].plot(ObscDr,likcDr.iloc[ii]['errDist'].squeeze(), '.')
    sns.histplot(like_stats_cDr_df.iloc[ii]['errDist'].squeeze(),
                 ax=ax13[1], stat='density', kde=False, color=clist[ii],
                 bins=75)
    ax13[1].plot(like_stats_cDr_df.iloc[ii]['errDist'].squeeze(),
                 like_stats_cDr_df.iloc[ii]['errDens'].squeeze(), '*',
                 color=clist[ii])
    #sns.kdeplot(likcDr.iloc[ii]['errDens'].squeeze(),ax=ax13[1], color=colors[ii])

    pd.plotting.autocorrelation_plot(like_stats_cDr_df.iloc[ii]['errDist'].squeeze(),
                                     ax=ax13[2], color=clist[ii])
    stats_c[ii, 0] = like_stats_cDr_df.iloc[ii]['errDist'].squeeze().mean()
    stats_c[ii, 1] = like_stats_cDr_df.iloc[ii]['errDist'].squeeze().std()

ax13[0].set_xlabel('concentration')
ax13[0].set_ylabel('residuals')
ax13[1].set_xlabel('residuals')
ax13[1].set_title(loc_name + r': concentration ($mg/L$]')
ax13[0].legend(leg_text2)
plt.tight_layout()
fig13.savefig(fig_path + 'errors_conc' + plt_ext,dpi = dpi_plt)

# print(stats_c)

# %%
# plt.close(fig14)
fig14, ax14 = plt.subplots(5, 1, sharex=True,
                           figsize = (15,10))
# plot qinf
[ax14[0].plot(iBB.trange[n2006+1:], qInf_tot[ii][n2006:], color=clist[ii])
 for ii in range(len(par_setdf))]

[ax14[1].plot(iBB.trange[n2006:], qev_tot[ii][n2006:], color=clist[ii])
 for ii in range(len(par_setdf))]

[ax14[2].plot(iBB.trange[n2006:], thCL_tot[ii][n2006:], color=clist[ii])
 for ii in range(len(par_setdf))]

[ax14[3].plot(iBB.trange[n2006:], np.maximum(0, (thCL_tot[ii][n2006:]-emLF_tot[ii].cL.thRes)
                                             / (emLF_tot[ii].cL.Por - emLF_tot[ii].cL.thRes)), color=clist[ii])
 for ii in range(len(par_setdf))]

[ax14[4].plot(iBB.trange[n2006:], mCL_tot[ii][n2006:], color=clist[ii])
 for ii in range(len(par_setdf))]

ax14[0].autoscale(enable=True, axis='x', tight=True)
ax14[0].set_ylabel(r'$q_{inf}$')
ax14[0].set_ylim(-0.04,0.005)
ax14[1].set_ylabel(r'$q_{ev}$')
ax14[2].set_ylabel(r'$\theta_{w_{cl}}$')
ax14[3].set_ylabel(r'$S_{eff}$')
ax14[4].set_ylabel(r'$M_{cl} \; [g/m^2$]')
#ax14[4].set_ylim(-0.001,0.5)
ax14[4].set_xlabel('time')
#ax14[0].set_xlim(iBB.tmeas[0],iBB.tmeas[-1])
ax14[4].legend(leg_text2, loc='upper right')

ax14[0].set_title(loc_name)
ax14[0].autoscale(enable=True, axis='x', tight=True)
fig14.savefig(fig_path + 'Coverlayer_' + plt_ext,dpi = dpi_plt)

# %%
# Plot forward travel time pdf
tage = np.arange(0.0001, 15*365, 0.1)
rv1 = np.zeros([len(par_setdf), len(tage)])
rv2 = np.zeros([len(par_setdf), len(tage)])
cdf = np.zeros([len(par_setdf), len(tage)])
pdf = np.zeros([len(par_setdf), len(tage)])
for ii in range(len(allpardf)):
    sig1 = 10**allpardf['wBsig1'].iloc[ii]
    sig2 = 10**allpardf['wBsig2'].iloc[ii]
    tau1 = allpardf['wBtau1'].iloc[ii]
    tau2 = allpardf['wBtau2'].iloc[ii]
    mfrac = allpardf['wBmfrac'].iloc[ii]
    rv1 = scpstats.lognorm(sig1, loc=0, scale=tau1)
    rv2 = scpstats.lognorm(sig2, loc=0, scale=tau2)
    cdf[ii] = mfrac*rv1.cdf(tage) + \
        (1-mfrac)*rv2.cdf(tage)
    pdf[ii] = mfrac*rv1.pdf(tage) + \
        (1-mfrac)*rv2.pdf(tage)

# fig15, ax15 = plt.subplots(1, 1, figsize=(17,13))
# [ax15.plot(tage, pdf[ii], color=clist[ii]) for ii in range(len(par_setdf))]
# ax15.autoscale(enable=True, axis='x', tight=True)
# ax15.set_ylabel('pdf')
# ax15.set_xlabel('tage')
# ax15.set_title(loc_name + ': probability distribution traveltime')

fig16, ax16 = plt.subplots(2, 1, sharex=True, figsize=(15,10))
# plot qinf
[ax16[0].plot(tage, cdf[ii], color=clist[ii]) for ii in range(len(par_setdf))]
#ax16[0].plot(tage, cdf[ii], color=clist[ii]) for ii in range(len(par_setdf))]
[ax16[1].plot(emLF_tot[ii].cdf_bF, color=clist[ii]) for ii in np.arange(len(par_setdf))]

ax16[0].autoscale(enable=True, axis='x', tight=True)
ax16[0].set_ylabel(r'$p_{q_{inf}}(T_E)$')
ax16[1].set_ylabel (r'$p_{q_{bF}}(T_E)$')
ax16[1].set_xlabel(r'$T_E$ [d]')

ax16[1].set_xlim(0,2500)
ax16[1].legend(leg_text2)

ax16[0].set_title(loc_name + ': traveltime distributions')

fig16.savefig(fig_path + 'cdf_traveltime_' + plt_ext,dpi = dpi_plt)

#  %%
fig17 = plt.figure(17, figsize=(15,10))
plt.clf()
[plt.plot(iBB.trange[n2006:], np.sum(vW_tot[ii][n2006:, :], axis=1),
          color=clist[ii]) for ii in range(len(par_setdf))]
plt.autoscale(enable=True, axis='x', tight=True)
plt.title(loc_name + ': total storage')
plt.xlabel('year')
plt.ylabel(r'$S_{WB}$ [m]')
plt.legend(leg_text2)

fig17.savefig(fig_path + 'total_storage_' + plt_ext,dpi = dpi_plt)
#g = sns.pairplot(data = sorteddf.iloc[:10000],plot_kws=dict(marker="+"))
# g.savefig('pairplot.png',dpi = dpi_plt)

# %%
fig18 = plt.figure(18, figsize=(15,10))
plt.clf()
for ii in range(len(par_setdf)):
    plt.plot(iBB.trange[n2006:], bF_tot[ii][n2006:], color=clist[ii])
plt.autoscale(enable=True, axis='x', tight=True)
plt.title(loc_name + ': base Flow ')
plt.xlabel('year')
plt.ylabel(r'$q_{b_F}(S_{bulk})$ [m/d]')
plt.legend(leg_text2)
fig18.savefig(fig_path + 'base_flow_' + plt_ext,dpi = dpi_plt)

# %%
# plots including uncertainty
# Cumulative flow
n2008 = 0  # np.where(iBB.trange=='2008-01-01')[0][0]
tds = iBB.tdseries
iflag = 'err'

fig19, ax19 = plt.subplots(nrows=3, ncols=1, sharex=True,
                           figsize = (15,10))
#fig20, ax20 = plt.subplots(nrows=1, ncols=1, sharex=True)


llist = []
ll = ax19[0].plot(iBB.meas_data_flow.index, iBB.meas_data_flow['totF']
         - iBB.meas_data_flow['totF'].iloc[iBB.tdseries.tcalmeas_ind[0]], 
         'o', color=clist[9], alpha=0.5)
llist.append(ll)

ax19[1].plot(iBB.tmeas, iBB.meas_data_flow.diff().values/
             iBB.measFreq, '.', color=clist[9], alpha=0.5)

ax19[2].plot(iBB.lab_data.index, iBB.lab_data['Cl'],
             '.', color=clist[9], alpha=0.5)

ax19[0].autoscale(enable=True, axis='x', tight=True)

for ii in range(len(par_setdf)):
    # work around to prevent a warning on copy of a slice from a DataFrame...
    likPartmp = likPar_qDr_df.iloc[ii].copy()
    likPartmp['m_min'] = emLF.BaseFlow(vW_tot[ii][:, -1])
    logLq, e_est, std_e_qDr, a_est, faq, ExpYq, SimYq, eq = mymod.generalizedLikelihood(
        iflag, likPartmp, qDr_tot[ii], 0)
    logLc, e_est, std_e_cDr, a_est, fac, ExpYc, SimYc, ec = mymod.generalizedLikelihood(
        iflag, likPar_cDr_df.iloc[ii], np.log10(cDr_tot[ii]+1e-12), 0)

    totF_sim_tmp = cumqDr_tot[ii][iBB.tmeas_ind] - cumqDr_tot[ii][iBB.tmeas_ind[0]]
    #ExpYq = like_stats_qDr2_df.iloc[ii]['ExpY']
    #ExpYc = like_stats_cDr_df.iloc[ii]['ExpY']

    std_errq = likPar_qDr_df.iloc[ii]['std0'] + likPar_qDr_df.iloc[ii]['std1']*(
        ExpYq - likPartmp['m_min'])
    std_errc = likPar_cDr_df.iloc[ii]['std0'] + likPar_cDr_df.iloc[ii]['std1']*(
        ExpYc - likPar_cDr_df.iloc[ii]['m_min'])

    std_errF = np.zeros(std_errq.shape)
    std_errF[n2008:] = np.sqrt(np.cumsum(std_errq[n2008:]**2))
    std_errF[:n2008] = np.average(std_errF[n2008:])

    ExpCF = np.cumsum(ExpYq)
    # SimCF = np.cumsum(SimYq)
    # ax19[0].plot(iBB.tmeas, totF_sim_tmp-totF_sim_tmp[iBB.tdseries.tcalmeas_ind[0]])
    
    #SimYq_mean = SimYq_mean - SimYq_mean[tds.tcalib_ind[tds.tmeascal_ind[0]]]
    #SimCF1 = SimCF - SimCF[tds.tcalib_ind[tds.tmeascal_ind[0]]]
    ExpCF1 = ExpCF - ExpCF[tds.tcalib_ind[tds.tmeascal_ind[0]]]
    
    ll = ax19[0].plot(iBB.trange[n2006:], ExpCF1[n2006:], color=clist[ii])
    llist.append(ll)
    
    ax19[0].fill_between(iBB.trange[n2006:], ExpCF1[n2006:]-1.96*std_errF[n2006:],
                         ExpCF1[n2006:]+1.96*std_errF[n2006:], color=clist[ii], alpha=0.1)
    
    ax19[1].plot(tds.trange[n2006:], ExpYq[n2006:], color=clist[ii])
    ax19[1].fill_between(tds.trange[n2006:], ExpYq[n2006:]-1.96*std_errq[n2006:],
                         ExpYq[n2006:]+1.96*std_errq[n2006:], color=clist[ii], alpha=0.1)
    
    ax19[2].plot(iBB.trange[n2006:], 10**ExpYc[n2006:]+1e-12, color=clist[ii])
    ax19[2].fill_between(iBB.trange[n2006:], 10**(ExpYc[n2006:]-1.96*std_errc[n2006:]),
                         10**(ExpYc[n2006:]+ 1.96*std_errc[n2006:]), color=clist[ii], alpha=0.1)
    # ax19[2].fill_between(iBB.trange[n2006:], 10**(np.log10(cDr_tot[ii][n2006:]+1e-12)-
    #                      1.96*std_errc[n2006:]),
    #                      10**(np.log10(cDr_tot[ii][n2006:]+1e-12)+
    #                      1.96*std_errc[n2006:]), color=clist[ii], alpha=0.1)
    

    
ax19[0].set_ylabel(r'cumulative leachate [$m$]')
ax19[0].set_title(loc_name)

ax19[1].set_ylabel(r'leachate pump rate [$m/day]$')
                           
ax19[2].set_xlabel('year')
ax19[2].set_ylabel(r'$[{Cl}^-]$ [mg/L]')

#create legend for lines and markers only!
from matplotlib.lines import Line2D
legend_elems = [Line2D([],[], linestyle=None, markerfacecolor = clist[9], alpha=0.5, marker = 'o',label = 'meas')]
legend_elems_qDr = [Line2D([],[], linestyle=None, markerfacecolor = clist[9], alpha=0.5, marker = 'o',label = 'meas')]
legend_elems_cum = [Line2D([],[], linestyle=None, markerfacecolor = clist[9], alpha=0.5, marker = 'o',label = 'meas')]
legend_elems_cDr = [Line2D([],[], linestyle=None, markerfacecolor = clist[9], alpha=0.5, marker = 'o',label = 'meas')]

for ii in range(len(par_setdf)):
    le = Line2D([],[], color = clist[ii], label = leg_text2[ii])
    le_qDr = Line2D([],[], color = clist[ii], label = leg_text2_qDr[ii])
    le_cum = Line2D([],[], color = clist[ii], label = leg_text2_cum[ii])
    le_cDr = Line2D([],[], color = clist[ii], label = leg_text2_cDr[ii])
    
    legend_elems.append(le)
    legend_elems_qDr.append(le_qDr)
    legend_elems_cum.append(le_cum)
    legend_elems_cDr.append(le_cDr)

ax19[0].legend(handles=legend_elems_cum, loc = 'lower right')
ax19[1].legend(handles=legend_elems_qDr, loc='upper right')
ax19[2].legend(handles=legend_elems_cDr)


fig19.savefig(fig_path + 'ForwardSim_total_' + plt_ext,dpi = dpi_plt)

n2028 = np.where(iBB.trange == '2028-01-01')[0][0]

ax19[2].set_xlim(iBB.tmeas[0],iBB.trange[n2028])
ax19[0].set_ylim([-1, 4.75])
ax19[1].set_ylim([-0.00025,0.005])
ax19[2].set_ylim([0, 6000])

fig19.savefig(fig_path + 'ForwardSim_calib_' + plt_ext,dpi = dpi_plt)

ax19[2].set_xlim(iBB.trange[n2006],iBB.trange[-1])
ax19[0].set_ylim([-4, 14])


ax19[0].legend(handles=legend_elems_cum, loc ='lower right')
ax19[1].legend(handles=legend_elems_qDr, loc='upper right')
ax19[2].legend(handles=legend_elems_cDr)

fig19.savefig(fig_path + 'ForwardSim_calib_extrap' + plt_ext,dpi = dpi_plt)

# %%
fig22 = plt.figure(22, figsize=(15,10))
plt.clf()
plt.plot(iBB.tmeas, iBB.meas_data_flow.diff().values /
         iBB.measFreq * iBB.lF.baseArea, '.', color=clist[9])
for ii in range(len(par_setdf)):
    plt.plot(iBB.tmeas[1:], np.diff(totF_sim[ii])/iBB.measFreq * iBB.lF.baseArea, color=clist[ii])
plt.autoscale(enable=True, axis='x', tight=True)
plt.xlabel('year')
plt.ylabel(r'leachate pump rate [$m3/day]$')
plt.title(loc_name)
plt.legend(leg_text1)

#plt.savefig(fig_path + 'Flow_' + plt_ext,dpi = dpi_plt)

# %%

# logp pydream is offset from one calculated by generalized likelihood
fig23, ax23 = plt.subplots(nrows=1, ncols=1, sharex=True,
                           figsize=(15,10))
sns.histplot(data=sorteddf['log_ps'],
                     stat='density', bins=15, kde=False,
                     cumulative=True,element='step',fill=False,
                     color='k', linewidth=2,
                     ax=ax23)
ax23.set_xlabel('likelihood')
ax23.set_title(loc_name)
fig23.savefig(fig_path + 'Sorted_log_ps' + plt_ext,dpi = dpi_plt)

# In[24] plot contribution of bulk in leachate 
# contribution bulk in pumped leachate
# conc( t_i) / bulk conc (t_i-1825)
fig24, ax24 = plt.subplots(nrows=5, ncols=1, sharex=True,
                           figsize = (15,10))
cRatio_tot = []
cBulk_tot = []
fRatio_tot = []

n2008 = 1825
for ii in range(len(par_setdf)):
    cBulk = mW_tot[ii][:,-1]/vW_tot[ii][:,-1]
    cOutflow = 10**(np.log10(cDr_tot[ii][n2008:]+1e-12))
    cRatio = cOutflow / cBulk[:-n2008]
    fRatio = bF_tot[ii][:-n2008]/qDr_tot[ii][n2008:]

    cRatio_tot.append(cRatio)
    fRatio_tot.append(fRatio)
    cBulk_tot.append(cBulk)

    ax24[0].plot(iBB.trange[n2008:], qDr_tot[ii][n2008:],
         '-', color=clist[ii], alpha=0.5)
    
    ax24[1].plot(iBB.trange[n2008:], cOutflow,
             '-', color=clist[ii], alpha=0.5)
    ax24[2].plot(iBB.trange[n2008:], cBulk_tot[ii][:-n2008],
             '-', color=clist[ii], alpha=0.5)
    ax24[3].plot(iBB.trange[n2008:], bF_tot[ii][:-n2008],
             '-', color=clist[ii], alpha=0.5)
    ax24[4].plot(iBB.trange[n2008:], fRatio,
             '-', color=clist[ii], alpha=0.5)
    
ax24[0].autoscale(enable=True, axis='x', tight=True)
ax24[0].set_ylabel(r'$q_w$')
ax24[0].set_title(loc_name)

ax24[1].set_ylabel(r'$C_{Drainage}$')

ax24[2].set_ylabel(r'$C_{Bulk}$')
ax24[3].set_ylabel(r'$bF$')
                           
ax24[4].set_ylabel(r'fRatio')
ax24[4].set_xlabel('year')


# In[25] plot total mass, vwater and concentration
fig25, ax25 = plt.subplots(3,1, sharex=True, figsize=(15,10))
mLF_tot = []
for ii in range(len(par_setdf)):
    mLF = mW_tot[ii].sum(axis=1) + mCL_tot[ii]
    mLF_tot.append(mLF)
    vWLF = thCL_tot[ii] * lFPar['cL'].dzCL + vW_tot[ii].sum(axis=1)
    cLF = mLF / vWLF
    ax25[0].plot(iBB.trange[n2006:], mLF[n2006:], 
                 color=clist[ii])
    ax25[1].plot(iBB.trange[n2006:], vWLF[n2006:], 
                 color=clist[ii])
    ax25[2].plot(iBB.trange[n2006:], cLF[n2006:],
                 color=clist[ii]) 

ax25[0].autoscale(enable=True, axis='x', tight=True)
ax25[0].set_title(loc_name + ': total mass LF')
ax25[0].set_ylabel(r'$M_{{tot}^-}$ [g/m2]')
ax25[1].set_ylabel(r'$S_{tot}$ [m3/m2]')
ax25[2].set_ylabel(r'$C_{tot}$ [mg/L]')
ax25[2].set_xlabel('year')

ax25[0].legend(leg_text2)
fig25.savefig(fig_path + 'total mass' + plt_ext,dpi = dpi_plt)

Emission_potential = [mLF_tot[ii][n2006] - mLF_tot[ii][-1] for ii in range(len(par_setdf))]
print ('Emission Potential: ', Emission_potential)

n2018 = np.where(iBB.trange == '2018-07-01')[0][0]
mLF2018 = [mLF_tot[ii][n2018] for ii in range(len(par_setdf))]
print ('TotalMass 2018: ', mLF2018)
# %%
# print(delta_ps)
# print(ExpYc)
# print(std_errc)
# print(likPar_cDr_df[['std0','std1']])
# print(iBB.tcalib)
# print(iBB.lF)
print(nburnin)

# %%
