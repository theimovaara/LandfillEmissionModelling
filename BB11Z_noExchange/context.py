# -*- coding: utf-8 -*-

import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


from landfillmodules import LFModelModules_v4 as mymod_v4

from landfillmodules import DataBaseLibrary as dbl


from landfillmodules import iniParRange_2TTi