"""
## Initialize_BB11Z_DREAM_LT.py

Initialization for landfill cell Braambergen 11Z
This version initializes the time data for long-term extrapolation.
This script is the intialization script for the landfill emission modelling code.
This script sets the site specific variables and then calls the database library
functions to import the site specific data.

Created on Sat Oct 20 2018
Modules for extracting data from the Chronos database

@author: T.J. Heimovaara
revision 20240603

"""

import numpy as np
import pandas as pd

from Initialize_BB11Z_DREAM01 import *

# meteo_data is top boundary condition. We run the model over 10 years
meteo_data = meteo_data[slice('2003-01-01','2024-10-31')]
meteo_data_tmp = meteo_data[slice('2003-11-01','2024-10-31')]

md2 = pd.concat([meteo_data,meteo_data_tmp],ignore_index=True)
md2 = pd.concat([md2,meteo_data_tmp],ignore_index=True)
#md2 = md2.append(meteo_data_tmp,ignore_index=True)

trange = pd.date_range('2003-01-01',periods=md2.shape[0],freq='D')
md2 = md2.set_index(trange)
meteo_data = md2

# Define simulation time range (trange)
#trange = pd.date_range(start='2008-01-01',end = '2019-06-30',freq='D')


# In order to facilitate quick and easy comparison of simulation with data
# we need to define the overlapping indices:
# tmeas_ind: trange[tmeas_ind] = tmeas
# tcalib_ind: trange[tcalib_ind]=tcalib
# tcalmeas_ind, tmeascal_ind: tmeas[tcalmeas_ind]=tcalib[tmeascal_ind]


xy, ind1, tmeas_ind = np.intersect1d(tmeas,  trange,
                                     return_indices=True)
xy, ind1, tcalib_ind = np.intersect1d(tcalib, trange,
                                      return_indices=True)
xy, tmeascal_ind, tcalmeas_ind = np.intersect1d(tcalib, tmeas,
                                                return_indices=True)

xy, tlabrange_ind, trangelab_ind = np.intersect1d(lab_data.index,  trange,
                                                return_indices=True)

xy, tlabmeas_ind, tmeaslab_ind = np.intersect1d(lab_data.index,  tmeas,
                                                return_indices=True)

xy, tmeascal_lab_ind, tcalmeas_lab_ind = np.intersect1d(tcalib, lab_data.index,
                                                        return_indices=True)


tdata = {'trange': trange,
         'tmeas': tmeas,
         'tcalib': tcalib,
         'tmeas_ind': tmeas_ind,
         'tcalib_ind': tcalib_ind,
         'tcalmeas_ind': tcalmeas_ind,
         'tmeascal_ind': tmeascal_ind,
         'tlabmeas_ind': tlabmeas_ind,
         'tmeaslab_ind': tmeaslab_ind,
         'tmeascal_lab_ind': tmeascal_lab_ind,
         'tcalmeas_lab_ind': tcalmeas_lab_ind,
         'tlabrange_ind': tlabrange_ind,
         'trangelab_ind': trangelab_ind}

tdseries = pd.Series(tdata)


