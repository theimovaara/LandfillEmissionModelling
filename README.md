# Quantification of the Emission Potential of Landfill Wastebodies using a Stochastic Leaching Framework

Project for calculating long-term water balance and emission of conservative solutes from the Afvalzorg iDS wastebodies.

Afvalzorg manages two landfills where we find four wastebodies that are used in the iDS landfill stabilization pilot. The landfill compartments are:
* Braambergen BB11N
* Braambergen BB11Z
* Braambergen BB12
* Wieringermeer WM01 (compartment 6).

This gitlab contains the python code to simulate the landfill emission.
The model package is found in *\landfillmodules*

The scripts to run find the parameter distributions with pyDREAM, evaluate output and plot graphs can be found in:
* *\BB11N_noExchange*
* *\BB11Z_noExchange*
* *\BB12_noExchange*
* *\WM01_noExchange*

Documentation and reference to functions (all in html) can be found in *\site*
Documentation is generated wiht mkdocs (source in *\docs*)


