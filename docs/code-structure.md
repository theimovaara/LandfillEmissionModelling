# Code structure for LandfillEmissionModelling

## Folder structure
The landfill emission model is script based and the model is run from a script. This has the advantage the model initialization can be tailored to the specific purpose of the simulation and that there is limited dependence on specific input files.
Currently scripts are available for simulation landfill emission from four distict landfill cells from two large sanitary engineered landfills operated by Afvalzorg NV: The Braambergen landfill close to Almere and the Wieringermeer landfill close to Medemblik, both in the Netherlands.

The scripts controlling the simulation are very similar but are site-specific. The scripts are organized in four subfolders:

- *./BB11N-noExchange/* containing the scripts for the Braambergen BB11N cell;
- *./BB11Z-noExchange/* containing the scripts for the Braambergen BB11Z cell;
- *./BB12-noExchange/* containing the scripts for the Braambergen BB12 cell;
- *./WM01-noExchange/* containing the scripts for the Wieringermeer cell number 6;

In these folders you find two sub-folders of importance:

- *./DataFiles/* containing compressed pickle files with the measured data for the landfill cell used for the data inference. This data has been downloaded from the iDS landfill database hosted at the TU Delft.
- *./Results_2TTi/* containing the inferred probability distributions for the unknown parameters. Data inference is carried out with the pyDREAM package where the likelihood is maximized for simulated leachate outflow and leachate concentrations with respect to measured time series.

The *./landfillmodules/* folder contains the packages with all functions used to run the landfill emission model and download the data (in not present in the *./DataFiles/* subfolders) from the iDS database.

The */docs/* folder contains the source files used for generating the model documentation which is a set of html files hosted in the */site/* folder. Finally */figures_i/* and *./dist-figs_i/* contain figures generated for the paper describing the details of this model.

!!! note "Reference"

    Heimovaara, T.J. and Wang, L, 2024 (submitted), Quantification of Emission Potential of Landfill Waste Bodies using a Stochastic Leaching Framework, *submitted to Water Resources Research*

## Scripts to run the code
The landfill emission model is an empirical model that is based on a large number of initially unknown parameters. The values of the parameters need to be inferred from measured data. We chose infer parameter distributions using the pyDREAM package.

Consequently, we have a two step process in the modelling effort:

1. First we infer parameters by matching simulated time series to measured datasets. Parameter inference is run using the *Optim_SL_\*.py* scripts in the wastebody subfolders.
2. After inferring the parameter distributions we use the *Eval_SL_\*.py* scripts to run the model, intepret model results and generate plots. This script allows extrapolation in to the future.

Two *Initialize_\*.py* scripts are used to prepare the measured data from the iDS database hosted by the TU Delft and match model output to measured data. The *LT* script defines the time parameters for extrapolation in to the future.

The *context.py* file facilitates importing modules from subdirectories found in the host directory.

The *make_plots_i.py* script runs the *Eval_SL_ \*.py* script and uses the data to generate the plots used for the paper (*Heimovaara and Wang, 2024*)

A more indepth explanation of how the scripts are built and what the other files in this folder are can be found in [Parameter optimization](parameter-optimization.md) and [Running code](running-code-with-parameter-set.md) in the User Guide.

## Modules with model functions
This folder contains three files:

- *DataBaseLibrary.py* which is a package with function allowing the downloading data from the iDS landfill database hosted by the TU Delft. Please note that this only works is you have access permissions.
- *LFModelModules_v4.py* which contains the full implementation of the landfill emission model and the implementation of the generalized likelihood method to calculated the likelihood values.
- *iniParRange_2TTi.py* which is a script that defines the prior parameter distributions for the landfill emission model. These priors are uniform distributions between a minimum and maximum value.

For people without access to the iDS database, thes functions will not work. The code is provided in order to provide the user with a deeper insight in to the data sets used to infer the model parameters.


