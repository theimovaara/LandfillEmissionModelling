# BB12_noExchange

::: BB12_noExchange.Eval_SL_BB12_DREAM02_longterm_i

::: BB12_noExchange.Initialize_BB12_DREAM01

::: BB12_noExchange.Initialize_BB12_DREAM_LT

::: BB12_noExchange.Optim_SL_BB12_DREAM02_2TTi

::: BB12_noExchange.context

## make_plots_i.py
Python script which runs the model for parameter sets by loading Eval_SL_BB12_DREAM02_longterm_i.
Then script proceeds to plot output for a wide range of results. Script generates all plots used in paper.

## Results_2TTi.tar.gz
Compressed dataset containing inferred parameter set for Braambergen 11N. After downloading, please unpack in to a subfolder: Results_2TTi

## optim_starts01.pkl
Pickle file with the last values of the parameter sets from a previous pyDREAM inference used to restart the pyDREAM inference.

## starts01.pkl
Pickle file with the last values of the parameter sets from the pyDREAM inference results stored in Results_2TTi.
