# Welcome to LandfillEmissionModelling

For full source and documentation visit [LandfillEmissionModelling](https://gitlab.tudelft.nl/theimovaara/LandfillEmissionModelling).

## Quantification of the Emission Potential of Landfill Wastebodies using a Stochastic Leaching Framework

Project for calculating long-term water balance and emission of conservative solutes from the Afvalzorg iDS wastebodies.

Afvalzorg manages two landfills where we find four wastebodies that are used in the iDS landfill stabilization pilot. The landfill compartments are:

* Braambergen BB11N
* Braambergen BB11Z
* Braambergen BB12
* Wieringermeer WM01 (compartment 6).

This gitlab contains the python code to simulate the landfill emission.
The model package is found in *\landfillmodules*

The scripts to run find the parameter distributions with pyDREAM, evaluate output and plot graphs can be found in:

* *\BB11N_noExchange*
* *\BB11Z_noExchange*
* *\BB12_noExchange*
* *\WM01_noExchange*

Documentation and reference to functions (all in html) can be found in *\site*
Documentation is generated wiht mkdocs (source in *\docs*)


## Project layout

        /landfilmodules # Package with modules for running model
        /BB11N_noExchange # Folder with scripts and data for the Braambergen BB11N compartment
        /BB11Z_noExchange # Folder with scripts and data for the Braambergen BB11Z compartment
        /BB12_noExchange # Folder with scripts and data for the Braambergen BB12 compartment
        /WM01_noExchange # Folder with scripts and data for the Wieringermeer compartment 6
        /docs # Folder for the mkdocs source files
        /dist_figs_i # Folder with plots for inferred parameter distributions
        /figures_i # Folder with plots used for paper
        /site # Folder contain html documentation
