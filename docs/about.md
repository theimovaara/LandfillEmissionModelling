# LandfillEmissionModelling
A Python package for simulating leachate production and leachate quality from engineered sanitary landfill wastebodies.
See User Guide for examples how to run the code. The code follows a two step approach:

1. Run the code with the pyDREAM package to infer parameters from a measured dataset;
2. Run the code with an inferred parameter set and plot graphs


author: T.J. Heimovaara
July 2024
