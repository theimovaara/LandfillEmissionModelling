# Getting started with LandfillEmissionModelling
LandfillEmissionModelling is a Python3 package. You need to install a python 3 environment. The code is verified to work with python=3.12.

Please clone or download the project from the main branch on: [LandfillEmissionModelling](https://gitlab.tudelft.nl/theimovaara/LandfillEmissionModelling)

The requirements.txt file gives a list of all packages required.

In the folders starting with BB11N, BB11Z, BB12 and WM01 you will find a Results_2TTi.tar.gz file. Please extract this compressedfile to the Results_2TTi sub folder. This folder contains the final inferred parameter distributions.

In order to test if the model runs correctly you can run the make_plots_i.py scripts. If all things have gone well, the code should generate 25 graphs.

Please refer to following paper (submitted) for more detailed explanation.

!!! note "Reference"

    Heimovaara, T.J. and Wang, L, 2024 (submitted), Quantification of Emission Potential of Landfill Waste Bodies using a Stochastic Leaching Framework, *submitted to Water Resources Research*
