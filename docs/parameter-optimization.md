# Parameter optimization

In order to run the model we need to define the values of the initially unknown parameters. This can be done by fitting the model output to measured data using standard optimization tools provided with scipy.

For the current implemetation we decided to infer the complete probability distribution of the parameters using pyDREAM. This section of the user guide gives a more detailed explanation of the *Optim_SL_\*.py* scripts and how the landfill emission model is integrated with pyDREAM.

The *Optim_SL_\*.py* scripts are derived from the example scripts provided in the installation folders of the pyDREAM package which needs to be installed with pip. The home page for pydream is hosted on github: [PyDREAM](https://github.com/LoLab-MSM/PyDREAM) The paper describing pyDREAM is:

!!! note "Reference"

    E. M. Shockley, J. A. Vrugt, and C. F. Lopez, “PyDREAM: High-dimensional parameter inference for biological models in python,” Bioinformatics, vol. 34, no. 4, 2018, doi: 10.1093/bioinformatics/btx626.

PyDREAM expects the forward model to calculate a likelihood which needs to be maximized. The *likelihood_Emission_LF* function in *LFModelModules_v4.py* calculates this likelihood value among a lot of others. The *Optim_SL_\*.py* script uses a wrapper function to only pass this likelihood value as logp back to calling function which in this case *run_dream* in lines 103 and 131 of the script. The wrapper function is defined as *def likelihood(par):*.

The script is initialized using the *iniParRange_2TTi.py* file from */landfillmodules/* imported as ipr. Here the folder name for storing the inferred parameters is defined as well as all the prior parameter distributions.

The DREAM_zs algorithm is run using parameter defined in lines 59 to 73 which should be self explanatory with the documentation of Shockley et al. (2018).
The other thing to realize is that the script can be tailored to start a fresh inference from the priors defined in *iniParRange_2TTi.py* but the script can also start inference from three specific parameter sets which can be defined by the user or from an earlier inference. It is also possible to restart inference using parameter distributions that have already been saved in the output folder.

The script checks if the parameter distributions in the different chains have converged to the same distribution usin the *Gelman_Rubin(oldsamples)* function and continues the inference until convergence. The script will run at least 25000 iterations.

The current version of the script starts from a set of three predefined parameter sets stored in *optim_starts01.pkl* which are the last values from the 3 chains from a previous inference. In this way the model quickly converges to a final distribution because near optimal parameter sets were used as an initial guess. During the inference the model starts to sample from the prior distribution, but the Metropolis Markov Chain alogrithm will discard low likelihood values.
