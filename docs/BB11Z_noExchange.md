# BB11Z_noExchange

::: BB11Z_noExchange.Eval_SL_BB11Z_DREAM02_longterm_i

::: BB11Z_noExchange.Initialize_BB11Z_DREAM01

::: BB11Z_noExchange.Initialize_BB11Z_DREAM_LT

::: BB11Z_noExchange.Optim_SL_BB11Z_DREAM02_2TTi

::: BB11Z_noExchange.context

## make_plots_i.py
Python script which runs the model for parameter sets by loading Eval_SL_BB11Z_DREAM02_longterm_i.
Then script proceeds to plot output for a wide range of results. Script generates all plots used in paper.

## Results_2TTi.tar.gz
Compressed dataset containing inferred parameter set for Braambergen 11N. After downloading, please unpack in to a subfolder: Results_2TTi

## optim_starts01.pkl
Pickle file with the last values of the parameter sets from a previous pyDREAM inference used to restart the pyDREAM inference.

## starts01.pkl
Pickle file with the last values of the parameter sets from the pyDREAM inference results stored in Results_2TTi.
