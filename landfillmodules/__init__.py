"""Modules for Landfill Emission Modelling.

Modules exported by this package:

- 'DatabaseLibrary': Provide functions to read data from the iDS_Landfill database
                    on the chronos01 server.
- 'iniParRage_2TTi': Define prior (uniform) distributions for parameters in Landfill Emission
                     model.
- 'LFModelModules_v4': Provides Landfill Emission Model functions

"""
