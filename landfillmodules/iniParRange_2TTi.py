#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
## iniParRange_2TTi.py

Script to define the uniform prior parameter distributions for 
the Landfill Emission Model

This module defines the following parameters:

- 'par_df' - dataframe with uniform priors (minimum, maximum and test values) for all parameters
- 'statpar_df' - dataframe with uniform priors for the generalized likelihood model
- 'results_dir (string)' - directory for storing inferred parameter distributions

Created on Tue Aug 31 09:56:12 2021
@author: theimovaara
Revised: Jul 3 2024


"""
import pandas as pd
import numpy as np

par_d = {#'dzCL': [0, 2.0, 0.9267], #%m
          'cropFact': [0.75, 1.5, 0.96, r'C_f'], #[-]
          'cLPor': [0.3, 0.5, 0.30, r'\theta_{w_{cl},max}'], #[-]
          'cLthRes': [0.0001, 1.0, 0.5, r'f_{w_{cl},min}'], # percentage of thTot
          'cLKSat': [-5, 3, 2.2,r'^{10}\log K_{cl}'], # 10 log
          'cLbpar': [0, 8, 8, r'^{10}\log b_{cl}'], #%10log! [m/d]
          'wBtau1': [1, 2*365, 31, r'\tau_{fast}'],
          'wBsig1': [-5, 2.5, 0.04, r'\sigma_{fast}'],
          'wBtau2': [0, 25*365, 1128, r'\Delta\tau_{slow}'],
          'wBsig2': [-5, 3, -1, r'\sigma_{slow}'],
          'wBmfrac': [0, 1.0, 0.6, r'\beta_f'],
          'wBthIni': [0, 0.5, 0.50, r'\theta_{wb_{ini}}'],
          'wBthMin': [0, 0.5, 0.1, r'f_{wb_{min}}'],
          'wBbFlow': [-4, -1.5,-3, r'^{10}\log bF_0'], #10log!
          #'wBiniBFlow': [0, 1, 0.1],
          'cLcIni': [-4, 6, 3, r'^{10}\log c_{ini_{cl}}'], #10log!
          'wBcIni': [2, 6, 2.6, r'c_{ini_{wb}}'],
          'wBpos_bF': [0, 15, 1, r'S_{bF}'], # gamma (shape)
          'wBsig_bF': [0, 15, 1, r'\sigma_{bF}'], # gamma (a)
          'wBgamm_ttf_bF': [-9, 0, -0.3, r'^{10}\log a_{bF}'], #10log! a in gamma distribution
          'wBtage_u': [0, 5*365, 1300, r't_{norm}'],
          'std0_qDr': [-8, 2, -4.4, r'^{10}\log \sigma_{0_{q_{Dr}}}'], #10log! 10% of minimum flux (0.0001 mm/day)
          'std1_qDr': [-8, 2, -0.85, r'^{10}\log \sigma_{1_{q_{Dr}}}'], #10log! limited to 10 %
          'beta_qDr': [-1, 1, 0.99, r'\beta_{q_{Dr}}'],
          'xi_qDr': [0.1, 10, 1.0, r'\xi_{q_{Dr}}'],
          #'mu1_qDr': [-8, 2, 128], #10log!
          'phi1_qDr': [-1, 0.7, 0.5, r'\phi_{q_{Dr}}'],
          #'phi2_qDr': [-1, 1, 0],
          #'phi3_qDr': [0, 1, 0],
          #'phi4_qDr': [0, 1, 0],
          #'bc_K_qDr': [0, 10, 0],
          #'bc_lambda_qDr':[-5, 5, 1],
          'std0_cDr': [-8, 2, -5.6, r'^{10}\log \sigma_{0_{c_{Dr}}}'], #10log! 10% of minimum value log10(300)
          'std1_cDr': [-8, 2, -1.5, r'^{10}\log \sigma_{1_{c_{Dr}}}'], #10log! limited to 10 %
          'beta_cDr': [-1, 1, 0.42, r'\beta_{c_{Dr}}'],
          'xi_cDr': [0.1, 10, 0.82, r'\xi_{c_{Dr}}'],
          #'mu1_cDr': [0, 1, 0.01], #10log!
          'phi1_cDr': [-1, 0.7, 0.9, r'\phi_{1_{c_{Dr}}}'],
          #'phi2_cDr': [-1, 1, 0],
          #'phi3_cDr': [0, 1, 0],
          #'phi4_cDr': [0, 1, 0],
          #'bc_K_cDr': [0, 1500, 0],
          #'bc_lambda_cDr':[-5, 5, 1]
          #'wBqMin': [-5, -2, -4],
          #'wBcMin': [0, 3, 2.5]
          }
par_df = pd.DataFrame(data=par_d)

statpar = {'std0_qDr': -10.,
            'std1_qDr': -10.,
            'beta_qDr': 0,
            'xi_qDr': 1,
            'mu1_qDr': 0,
            'phi1_qDr': 0,
            'phi2_qDr': 0,
            'phi3_qDr': 0,
            'phi4_qDr': 0,
            'bc_K_qDr': 0,
            'bc_lambda_qDr': 1,
            'std0_cDr': -10.,
            'std1_cDr': -10.,
            'beta_cDr': 0,
            'xi_cDr': 1,
            'mu1_cDr': 0,
            'phi1_cDr': 0,
            'phi2_cDr': 0,
            'phi3_cDr': 0,
            'phi4_cDr': 0,
            'bc_K_cDr': 0,
            'bc_lambda_cDr': 1,
            'wBqMin': -12,  # log10!
            'wBcMin': 0
            }
statpar_df = pd.DataFrame(data=statpar,index=[0])

results_dir = './Results_2TTi/'
    
