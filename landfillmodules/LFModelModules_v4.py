#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
## LFModelModules_v4.py
Main module for the LandfillEmissionModelling python code

This module contains the following:

- 'Emission_LF' - class with all functions to run the model
- 'generalizedLikelihood(iflag, likPar, ModY, ObsY)' -  function to calculate the generalized likelihood
            of a simulation with respect to set of observations 
- 'likelihood_Emission_LF(par, simType, tdseries, lF, meteo_data,
                           meas_data_flow, lab_data, par_df, statpar_df, 
                           ntt = 5*365,flow_only=False)' - function to initialize and run the
                           Landfill Emission Model.

Created on Sun Dec 10 09:46:35 2018
@author: theimovaara
Revised: Jul 3 2024 (comments)

"""

import numpy as np
#import scipy.integrate as spi
import scipy.stats as stats
import pandas as pd
import scipy.special as spspec
import scipy.signal as spsig


import matplotlib.pyplot as plt
import seaborn as sns
#from context import like


class Emission_LF:
    """ Class for simulating water and mass balance in landfill waste bodies.
    
    Model allows of recirculation of leachate.
    Boundary conditions are controlled by rainfall, potential evapo-transpiration,
    leachate infiltration, concentration in infiltrated leachate.

    Args:
        tRange (np.array(np.int64)): list of output times for simulation
        iniSt (pd.Series): initial values for model states
        cL (pd.Series): model parameters for coverlayer simulation
        wB (pd.Series): model parameters for waste body simulation
        lF (pd.Series): model parameters for landfill 
        meteo_data (): boundary values for precipitation and evapo-transpiration
        infilF ()': not yet implemented, boundary values for infiltration data.
    """

    # TO DO: implement concentration in infiltrating leachate (recirculation)
    #       mass in coverlayer increases due to infiltrating leachate
    #       mass in cover layer decreases with runoff and infiltration to waste
    #       body
    #       Assumption: All flows leaving cover layer have same concentration!

    def __init__(self, tRange, iniSt, cL, wB, lF, meteo_data, infilF):
    
        # Initialize emission models
        self.tRange = tRange
        self.dt = np.diff(tRange)
        iniSt = iniSt
        self.cL = cL
        self.wB = wB
        self.lF = lF
        meteo_data = meteo_data
        self.infilF = infilF

        self.ndays = int(tRange[-1]-tRange[0])
        self.ndays_tracer = cL.tracer_day - tRange[0]
        ndays = self.ndays
        self.dt = np.diff(tRange)

        # Variables for Cover Layer model
        self.th = np.zeros([ndays+1])
        self.seff = np.zeros([ndays+1])
        self.cInf = np.zeros([ndays+1])
        self.m = np.zeros([ndays+1])
        self.qInf = np.zeros([ndays])
        self.qrf = np.zeros([ndays])
        self.qev = np.zeros([ndays])
        # self.qinfil = np.zeros([ndays])
        # self.cinfil = np.zeros([ndays+1])
        # self.qrunoff = np.zeros([ndays])
        self.mbalerr = np.zeros([ndays+1])

        #self.thMin = 0.001
        # save initial states
        self.th[0] = iniSt.thIni
        self.m[0] = iniSt.cLcIni*self.th[0]

        # select rainfall and evaporation data from meteo_data
        meteo_time = meteo_data.index.view(np.int64)/(1e9 * 3600 * 24)
        xy, md_ind, t_ind = np.intersect1d(
            meteo_time, np.ceil(tRange), return_indices=True)

        self.qrf = -meteo_data['rain_station'].iloc[md_ind].values
        self.qev = meteo_data['pEV'].iloc[md_ind].values * cL.cropFact

        
        # Initialize Wastebody Emission Model
        # number of mobile cells...
        ndEx = int(wB.ndEx)
        tage = np.arange(ndEx+1)

        # Double lognormal residence time probabability function for infiltrating water
        rv1 = stats.lognorm(wB.sig1, loc=0, scale=wB.tau1)
        rv2 = stats.lognorm(wB.sig2, loc=0, scale=wB.tau2)

        cdf1 = rv1.cdf(tage)
        cdf2 = rv2.cdf(tage)
        self.cdf = wB.mfrac*cdf1 + (1-wB.mfrac)*cdf2
        self.pdf = np.concatenate(([0], np.diff(self.cdf)))

        # Gamma probability function for residence time of base-flow
        rv5 = stats.gamma(a=self.wB.gamm_ttf_bF, scale=1)
        #rv5 = stats.gamma(a=0.0001, scale=1)
        # base flow flipped in time and normalized so that cdf reaches 1. 
        cdf_bF = rv5.cdf((wB.ndEx-tage)/self.wB.tage_u) \
            / rv5.cdf((wB.ndEx)/self.wB.tage_u) 
            
        self.cdf_bF = np.concatenate((cdf_bF, [0]))
        self.pdf_bF = -np.diff(self.cdf_bF)

        # Initialize water volume and solute mass vectors ...
        self.vW = np.zeros([ndays+1, ndEx+1])
        self.mW = np.zeros([ndays+1, ndEx+1])

        # Initialize gamma function for calculating base flow
        # self.wB.a_bF = 1
        self.dist_bF = stats.gamma(self.wB.sig_bF, scale=self.wB.pos_bF, 
                                   loc=0)
        # Initialize initial values of water volume to initial base flwo
        bF = self.BaseFlow(iniSt.wBvWIni)
        self.vW[0, 0:ndEx] = bF  # wB.bFlow  # iniSt.wBvWIni * wB.mfrac / ndEx
    
        # Initialize bulk water storage
        self.vW[0, ndEx] = iniSt.wBvWIni #- ndEx * wB.bFlow

        # Initialize mass in residence time cells and bulk storage
        self.mW[0, 0:ndEx] = self.vW[0, 0:ndEx] * iniSt.wBcIni
        self.mW[0, ndEx] = self.vW[0, ndEx] * iniSt.wBcIni
            
        
    def BaseFlow(self, vWBulk):
        """ Function to calculate base flow as a function of bulk water storage...
        
        Args:
            vWBulk (np.array): water storage volume in bulk
        
        """
        f = (self.wB.bFlow * self.dist_bF.cdf(vWBulk-self.wB.vWMin))
        return f


    def ExRates(t, mW, vW, kEx, ndEx, bFlow):
        """ Calculate the solute exchange between cell and bulk...
        
        Args:
            t (np.array): array with simulation times
            mW (np.array): array with solute masses in residence time cells
            vW (np.array): array with water volumes in residence time cells
            kEx (float): exchange parameter (1/d)
            ndEx (int): number of residence time cells
            bFlow (float): base flow rate
        """
        totM = np.sum(mW)
        totV = np.sum(vW)
        dMdt = np.zeros(ndEx+1)
        jj = np.arange(0, ndEx)
        dMdt[jj] = kEx*(vW[jj]*totM/totV - mW[jj].squeeze())
        dMdt[ndEx] = -np.sum(dMdt[jj])
        return dMdt


    def Emission_CL(self, th, m, qrf, qev, dt):
        """ Function for calulating water and mass balance of cover layer
        
        Args:
            th (float): volumetric water content in layer [-]
            m (float): mass in layer kg/m2
            qrf (float): rainfall rate m3/(m2 day)
            qev (float): potential evapo transpiration rate m3/(m2 day)
            dt (float): time step (day)
        """

        thMin = 0.001
        seff = np.maximum(0, (th-self.cL.thRes)/(self.cL.Por-self.cL.thRes))
        qInf = -self.cL.KSat * seff**self.cL.bPar
        thEst = th - (qrf - qInf + qev) * dt / self.cL.dzCL
        if thEst > self.cL.Por:
            # qInf too small, profile is too wet, excess rainfall should be routed
            # to infiltration flux
            thExcess = thEst-self.cL.Por

            qInfFast = (1.0 - self.cL.alphaRO) * thExcess * self.cL.dzCL / dt
            qInf = qInf - qInfFast
            #self.qinf[ii] = self.qinf[ii]
            thEst = th - (qrf - qInf + qev) \
                * dt / self.cL.dzCL
        else:
            if (thEst < thMin) and (th > self.cL.thRes):
                # qInf is too large and possible qEvap as well We start by reducing
                # qInf
                qInf = -(th-self.cL.thRes)*self.cL.dzCL / dt
                # restimate thEst
                thEst = th - (qrf - qInf + qev) * dt / self.cL.dzCL

            if thEst < thMin:
                # only qEv is too large and needs to be reduce
                dqev = (thMin-thEst)*self.cL.dzCL/dt
                qev = qev - dqev
                thEst = th-(qrf - qInf + qev) * dt / self.cL.dzCL

        thnew = np.maximum(thEst, thMin)
        mbalerr = thEst - (th - (qrf - qInf + qev) * dt / self.cL.dzCL)

        cInf = m / (self.cL.dzCL * np.maximum(th, thMin))
        
        
        # solve first order rate equation to estimate remaining mass in cL
        # will not work for inifiltration system (then we need to solve cover-
        # layer as differential equation). Now it works because cRain = 0 and
        # qrunoff = 0
        rate =  qInf * cInf 
        # print ('cInf = ', cInf, 'm = ', m, 'rate = ', rate, 
        #        'qInf = ', qInf)
        if np.isnan(rate):
            rate = 0
            
        #print('rate = ', rate)
        #mnew = m * np.exp(rate*dt)
        mnew = m + rate*dt

        if np.isnan(mnew):
            mnew = 0
 
        #mnew = np.maximum(0,m - (qrf * self.cL.cRain - qInf *cInf)* dt / self.cL.dzCL)

        #seff[ii+1] = np.maximum(0,(th[ii+1]-self.cL.thRes)/(self.cL.Por-self.cL.thRes))
        #qinf[ii+1] = -cL.KSat * seff[ii+1]**cL.bPar

        cLOut = [qInf, thnew, mnew, cInf, mbalerr, qev]
        return cLOut

    
    def Emission_WB(self, vW, mW, qInf, cInf):
        """ Function for calulating water and mass fluxes of Waste body for a
        single time step.

        The waste body is discretized in ndEx cells (one for each residence time, where last cell 
        is the bulk).
        Each cell contains water jj days from being removed from the waste body
        as pumped leachate.

        Args:
            vW (np.array): volume of water stored in waste body m3/m2
            mW (np.array): mass stored in cell: kg/m2
            qInf (np.array): infiltration rate m3/(m2 day)
            cInf (np.array): concentration in infiltrating water (kg/m3)
            
        """

        ndEx = int(self.wB.ndEx)
        #tage = np.arange(ndEx)

        vWNew = np.zeros(ndEx+1)
        mWNew = np.zeros(ndEx+1)
        jj = np.arange(0, ndEx)
        kk = np.arange(0, ndEx-1)
        
        # solve water balance (water of ttravel time jj+1 + new version of rain)

        # shift all cells by one day, extract bF from bulk
        # please note last cell is zero and will be filled later from base flow
        vWNew[kk] = vW[kk+1]
        mWNew[kk] = mW[kk+1]

        bF = self.BaseFlow(vW[ndEx])
        #vWNew[ndEx-1] = bF  # bF is added using pdf
        vWNew[ndEx] = vW[ndEx]
        mWNew[ndEx] = mW[ndEx]

        # distribute rain over cells
        # please note age expetance of cells decreases by 1
        # we implement this by cells(jj) are cells(jj+1)
        vWNew[jj] = vWNew[jj] - qInf*self.pdf[jj+1] \
            + bF*self.pdf_bF[jj]

        vWNew[ndEx] = vWNew[ndEx] - qInf*(1-self.cdf[-1]) \
            - np.sum(bF*self.pdf_bF[jj])
        
        # mass flowing from bulk to mobile
        cBulk = mW[ndEx] / (vW[ndEx])
        
        # please note qInf is negative (downward flus from cover layer)
        mWNew[jj] = mWNew[jj] - qInf*self.pdf[jj+1]*cInf \
            + bF*self.pdf_bF[jj]*cBulk

        mWNew[ndEx] = mWNew[ndEx] - qInf*(1-self.cdf[-1])*cInf \
            - np.sum(bF*self.pdf_bF[jj]*cBulk)

        
        # Update mass in bulk
        # Ensure that mass in bulk storage is never less than zero...
        mWNew[ndEx] = np.amax(mWNew[ndEx], 0)
        #mWNew[ndEx] = mW[ndEx] - mWNew[ndEx-1]

        return vWNew, mWNew

    def SimulateEmissionLF(self):
        ''' Function for simulation emission from landfill waste body.

        The function loops through time and calls self.Emission_CL and 
        self.Emission_WB to model the emission with leachate. 
        Model results are parsed in to a list: emOUT

        Returns:
            emOUT (list): list consisting of: 
                          [cumqDr, cDr, qDr,  self.qInf, self.qev, self.th, self.m, self.cInf,
                           self.mbalerr, self.vW, self.mW]
        '''

        for ii in np.arange(0, self.ndays):

            if self.cL.tracer_test == True:
                if self.ndays_tracer == ii:
                    # add mass to cover layer ("unit"/m2).
                    self.m[ii] = self.cL.tracer_mass

            cLOut = self.Emission_CL(self.th[ii], self.m[ii], self.qrf[ii],
                                     self.qev[ii],
                                     self.dt[ii])

            self.qInf[ii], self.th[ii+1], self.m[ii+1], self.cInf[ii], \
                self.mbalerr[ii], self.qev[ii] = cLOut

            self.vW[ii+1], self.mW[ii+1] = self.Emission_WB(self.vW[ii],
                                                            self.mW[ii], 
                                                            self.qInf[ii], 
                                                            self.cInf[ii],
                                                            )

            
        qDr = self.vW[:, 0]
        cDr = self.mW[:, 0] / (self.vW[:, 0] + 1e-14)

        cumqDr = np.concatenate(([0], qDr), axis=0).cumsum()

        emOut = [cumqDr, cDr, qDr,  self.qInf, self.qev, self.th, self.m, self.cInf,
                 self.mbalerr, self.vW,
                 self.mW]

        return emOut


def generalizedLikelihood(iflag, likPar, ModY, ObsY):
    """ Pyhton implemenation of the GL.m function from the matlab DREAM toolbox
    function [logL,a,fa,ExpY,SimY] = GL(iflag,statpar,ModY,ObsY)

    Log-likelihood and simulation of regression models with correlated, heteroscedastic and non-Gaussian errors
    
    Regression model:

    1. Expected values: simulated using an external function (name specified by modname), with optional bias correction
    
    2. Residual standard deviations: modeled as a linear function of expected values
    
    3. Residual higher-order moments: modeled by Skew Exponential Power (SEP) distribution
    
    4. Residual correlations: modeled by autoregressive (AR) time-series model (implemented up to order 4)
    
    Reference: 
        G. Schoups and J. A. Vrugt, 2009, “A formal likelihood function for parameter and predictive inference of hydrologic models with correlated, heteroscedastic, and non-Gaussian errors,” Water Resources Research, vol. 46, p. 2009WR008933, Oct. 2010, doi: 10.1029/2009WR008933.

    Args:
        iflag (str): flag for estimation ('est') or simulation ('sim')
        ModY (np.array): column vector of modeled response variables from deterministic model
        ObsY (np.array): column vector of observed response values
        likPar (dict): {'std0': par[29],
                        'std1': par[30],
                        'beta': par[31],
                        'xi': par[32],
                        'mu1': par[33],
                        'phi1': par[34],
                        'phi2': par[35],
                        'phi3': par[36],
                        'phi4': par[37],
                        'bc_K': par[38],
                        'bc_lambda': par[39]
                        }
    
    Returns: 
        logL (float): log-likelihood function value
        a (np.array): column vector of i.i.d. errors distributed as SEP(0,1,xi,beta)
        fa (np.array): column vector with density values of errors a
        ExpY (np.array): column vector of expected values of response variables (after optional bias correction)
        SimY (np.array): column vector of simulated values of response variables (incl. simulated residuals)
    
    


    """

    # Separate deterministic and statistical parameters
    std0 = likPar['std0']  # intercept of linear heteroscedastic model
    std1 = likPar['std1']  # slope of linear heteroscedastic model
    # kurtosis parameter (-1: uniform, 0: normal; 1: Laplace)
    beta = likPar['beta']
    # skewness parameter (1: symmetric; <1: negative skew; >1: positive skew)
    xi = likPar['xi']
    mu1 = likPar['mu1']  # parameter for bias correction
    phi1 = likPar['phi1']  # first-order autoregressive coefficient
    phi2 = likPar['phi2']  # %second-order autoregressive coefficient
    phi3 = likPar['phi3']  # third-order autoregressive coefficient
    phi4 = likPar['phi4']  # fourth-order autoregressive coefficient
    bc_K = likPar['bc_K']  # %Box-Cox transformation parameter (skewness)
    # %Box-Cox transformation parameter (heteroscedasticity)
    bc_lambda = likPar['bc_lambda']
    m_min = likPar['m_min']  # minimum value in measurement data set

    N = len(ModY)

    # Bias
    #ModY = ModY.reshape(N,1)
    ExpY = ModY * np.exp(mu1*ModY)  # bias  eq2

    # STANDARD DEVIATIONS
    tmp = std1*(ExpY - m_min)
    std_e = tmp * (tmp > 0) + std0
    
    # KURTOSIS AND SKEWNESS (SEP parameters)
    A1 = spspec.gamma(3*(1+beta)/2)
    A2 = spspec.gamma((1+beta)/2)
    Cb = (A1/A2)**(1/(1+beta))
    Wb = np.sqrt(A1)/((1+beta)*(A2**(1.5)))
    M1 = spspec.gamma(1+beta)/np.sqrt(A1*A2)
    M2 = 1
    mu_xi = M1*(xi-1/xi)
    sig_xi = np.sqrt((M2-M1**2)*(xi**2 + 1/xi**2) + 2*M1**2 - M2)

    # CORRELATION
    phi_p = [1, -phi1, -phi2, -phi3, -phi4]  # coefficients of AR polynomial

    # ESTIMATION: compute log-likelihood
    SimY = []

    # %Residuals (e)
    if (iflag == 'est' or iflag == 'sim'):
        #ObsY = ObsY.reshape(N,1)
        if np.abs(bc_lambda) < 1e-8:
            e_est = np.log(ObsY+bc_K) - np.log(ExpY+bc_K)
        else:
            e_est = ((ObsY+bc_K)**bc_lambda - 1)/bc_lambda - \
                ((ExpY+bc_K)**bc_lambda - 1)/bc_lambda

        # i.i.d. errors (a)
        # y = lfilter(b,a,x,axis=0)
        # std_e*a(t) = 1*e(t) - phi1*e(t-1)
        # x = e(t)
        b_vec = phi_p
        a_vec = [1, 0, 0, 0]
        std_e_x_a = spsig.lfilter(b_vec, a_vec, e_est, axis=0)
        a_est = std_e_x_a / std_e

        # Log-likelihood
        a_xi = (mu_xi + sig_xi*a_est) / (xi**np.sign(mu_xi + sig_xi*a_est))
        
        logL = N * np.log(Wb*2*sig_xi / (xi+1/xi)) - np.sum(np.log(std_e)) \
            - Cb*(np.sum(np.abs(a_xi)**(2/(1+beta))))
        logL = logL + (bc_lambda-1)*np.sum(ObsY+bc_K)
        # Density of a
        fa = (2*sig_xi/(xi + 1/xi)) * Wb * \
            np.exp(-Cb*(np.abs(a_xi)**(2/(1+beta))))
    else:
        logL = []
        fa = []
        a_est = []
        e_est = []

    # SIMULATION: generate response variables (SimY) and estimated errors (e)

    if (iflag == 'sim' or iflag == 'err'):
        np.random.seed()  # initialize random number generators
        # Generate N i.i.d. errors (a) from skew exponential power distribution, SEP(0,1,xi,beta)
        # Step 1 - Generate N random variates from gamma distribution with shape parameter 1/p and scale parameter 1
        p = 2/(1+beta)
        grnd = np.random.gamma(1/p, 1, size=[N])

        # Step 2 - Generate N random signs (+1 or -1) with equal probability
        signrnd = np.sign(np.random.rand(N)-0.5)

        # %Step 3 - Compute N random variates from EP(0,1,beta)
        EP_rnd = signrnd * (np.abs(grnd)**(1/p)) * \
            np.sqrt(spspec.gamma(1/p)) / np.sqrt(spspec.gamma(3/p))

        # Step 4 - Generate N random signs (+1 or -1) with probability 1-w and w
        w = xi/(xi+1/xi)
        signrndw = np.sign(np.random.rand(N)-w)

        # Step 5 - Compute N random variates from SEP(mu_xi,sig_xi,xi,beta)
        SEP_rnd = -signrndw * np.abs(EP_rnd) / (xi**signrndw)

        # Step 6 - Normalize to obtain N random variates from SEP(0,1,xi,beta)
        a = (SEP_rnd-mu_xi) / sig_xi

        #Residuals (e)

        # e = spsig.lfilter(b, a, std_e*a, axis=0)
        # y = lfilter(b,a,x,axis=0)
        # 1*e_t = std_e*a(t) + phi1*e(t-1)
        # x = std_e*a(t)
        b_vec = [1, 0, 0, 0]
        a_vec = phi_p
        e = spsig.lfilter(b_vec, a_vec, std_e*a, axis=0)

        # Simulated values, assumption: E[g^-1(Y)] = g^-1(E[Y]) where g = Box-Cox transformation
        if np.abs(bc_lambda) < 1e-8:
            SimY = np.exp(np.log(ExpY+bc_K)+e) - bc_K
        else:
            SimY = (bc_lambda * (((ExpY+bc_K)**bc_lambda - 1) /
                    bc_lambda + e) + 1)**(1/bc_lambda) - bc_K
    else:
        a = []
        e = []

    return logL, e_est, std_e, a_est, fa, ExpY, SimY, e


def likelihood_Emission_LF(par, simType, tdseries, lF, meteo_data,
                           meas_data_flow, lab_data, par_df, statpar_df, 
                           ntt = 5*365,flow_only=False):

    ''' Function for running LandfillEmissionModel.
    This is the function used to simulating leachate production and leachate quality for a specific
    parameter set.
    
    The function returns all simulated values together with a the log-likelihoods.

    Args:
        par (np.array): parameter set
        simType (str): string defining type of simulation ('ForwardSim' for forward modelling or 'est' for paramter inference).
        tdseries (dict): dictionary containing simulation time range and indexes for matching simulation results to
                         measured data sets.
        lF (dict): dictionary containing specific data for waste body 
        meteo_data (pd.DataFrame): dataframe containing meteorological data on a daily basis 
        meas_data_flow (pd.DataFrame): dataframe containing cumulative leachate outflow
        lab_data (pd.DataFrame): dataframe containing laboratory measurements (for Chloride)
        par_df (pd.DataFrame): dataframe containing uniform prior parameter distributions
        statpar_df (pd.DataFrame): dataframe with parameter values for generalized likelihood function
        ntt (int): numer defining size of residence time arrays in waste body (default = 5*365)
        flow_only (boolean): boolean indicating if model should simulate only flow (default = False)

    Returns:
        logp (float): total log likelihood for all measured data sets
        emOut (list): list with all simulated outputs 
        lFPar (list): list containing all parameters used for simulation 
        emLF (class): initialized class for specific parameter set
        like_stats_qDr2 (list): list with results from generalized likelihood calculation for drainage data
        like_stats_cDr (list):  list with results from generalized likelihood calculation for concentration data
        likPar_qDr (list): list with likelihood parameters for drainage data
        likPar_cDr (list): list with likelihood parameters for concentration data
    '''


    # Using parameter values in par we initialize the model class
    dzCL = lF.cl_height  # m par[0]
    
    
    cropFact = par[0]
    cLPor = par[1]
    cLthRes = par[2]*cLPor
    cLKSat = 10**par[3]
    cLbPar = par[4]

    wBtau1 = par[5]
    wBsig1 = 10**par[6]
    wBtau2 = wBtau1 + par[7]
    wBsig2 = 10**par[8]
    wBmfrac = par[9]
    wBvWIni = par[10] * (lF.height - dzCL)
    wBvWMin = par[11]*wBvWIni
    wBbFlow = 10**par[12]

    cLcIni = 10**par[13]
    wBcIni = 10**par[14]

    wBpos_bF = par[15]
    wBsig_bF = par[16]
    wBgamm_ttf_bF = 10**par[17]
    wBtage_u = par[18]

    np1 = 18
    npar = par_df.shape[1]
    
    trange_sim = tdseries.trange.view(np.int64)/(1e9 * 3600 * 24)

    deltaT = np.diff(
        trange_sim[tdseries.tcalib_ind[tdseries.tmeascal_ind]]).mean()
    
    
    # Replace the values in stat par with optimized value
    for ii in np.arange(np1+1, npar):
        statpar_df[par_df.columns[ii]] = par[ii]

    qDr_min = 10**statpar_df['wBqMin'].values[0].astype(float)
    cDr_min = statpar_df['wBcMin'].values[0]
    # cDr_min = 10**statpar_df['wBcMin'].values[0]
    
    likPar_qDr = {'std0': 10.**statpar_df['std0_qDr'].values[0],
                  'std1': 10.**statpar_df['std1_qDr'].values[0],
                  'beta': statpar_df['beta_qDr'].values[0],
                  'xi': statpar_df['xi_qDr'].values[0],
                  'mu1': statpar_df['mu1_qDr'].values[0],
                  'phi1': statpar_df['phi1_qDr'].values[0],
                  'phi2': statpar_df['phi2_qDr'].values[0],
                  'phi3': statpar_df['phi3_qDr'].values[0],
                  'phi4': statpar_df['phi4_qDr'].values[0],
                  'bc_K': statpar_df['bc_K_qDr'].values[0],
                  'bc_lambda': statpar_df['bc_lambda_qDr'].values[0],
                  'm_min': qDr_min,
                  }


    # Please note autocorrelation parameters same as qDr
    likPar_cDr = {'std0': 10**statpar_df['std0_cDr'].values[0],
                  'std1': 10**statpar_df['std1_cDr'].values[0],
                  'beta': statpar_df['beta_cDr'].values[0],
                  'xi': statpar_df['xi_cDr'].values[0],
                  'mu1': statpar_df['mu1_cDr'].values[0],
                  'phi1': statpar_df['phi1_cDr'].values[0],
                  'phi2': statpar_df['phi2_cDr'].values[0],
                  'phi3': statpar_df['phi3_cDr'].values[0],
                  'phi4': statpar_df['phi4_cDr'].values[0],
                  'bc_K': statpar_df['bc_K_cDr'].values[0],
                  'bc_lambda': statpar_df['bc_lambda_cDr'].values[0],
                  'm_min': cDr_min,
                  }

    # number of discrete time cells (fixed)
    wBndEx = ntt #5*365

    cLthIni = cLPor/2
    #stdMeas_outF = sdData[0]
    #stdMeas_cDr = sdData[1]

    tracer_date = pd.to_datetime('2023-07-01', format='%Y-%m-%d')
    
    coverLayer = {'dzCL': dzCL,
                  'cropFact': cropFact,
                  'Por': cLPor,
                  'thRes': cLthRes,
                  'KSat': cLKSat,
                  'bPar': cLbPar,
                  'cRain': 0.0,
                  'alphaRO': 0,
                  'tracer_test': False,
                  'tracer_mass': 1, # kg/m2
                  'tracer_date': tracer_date,
                  'tracer_day': tracer_date.value/(1e9 * 3600 * 24)
                  }

    cL = pd.Series(coverLayer)

    # wasteBody = {'tau1': wBtau1,
    #           'sig1': wBsig1,
    #           'tau2': wBtau2,
    #           'sig2': wBsig2,
    #           'mfrac': wBmfrac,
    #           'bFlow': wBbFlow,
    #           'vWRes': wBvWRes,
    #           'vWK': wBvWK,
    #           'alphavW': wBalphavW,
    #           'ndEx': wBndEx
    #           }
    wasteBody = {'tau1': wBtau1,
                 'sig1': wBsig1,
                 'tau2': wBtau2,
                 'sig2': wBsig2,
                 'mfrac': wBmfrac,
                 'bFlow': wBbFlow,
                 'vWIni': wBvWIni,
                 'vWMin': wBvWMin,
                 'pos_bF': wBpos_bF,
                 'sig_bF': wBsig_bF,
                 'gamm_ttf_bF': wBgamm_ttf_bF,
                 'tage_u': wBtage_u,
                 'ndEx': wBndEx,
                 'drApar': 0,  # parameters for infiltration pdf. if no infiltration no effect
                 'drBpar': 0,
                 'drCpar': 0,
                 'tau3': 1000,
                 'sig3': 1,
                 'tau4': 1000,
                 'sig4': 1,
                 'mfrac2': 0.5
                 }
    wB = pd.Series(wasteBody)
    
    # Calculate initial volume in drainage system. Please note that the bottom liner is installed at a slope

    # if we are running a tracer test, ensure that no other solutes are present in the system
    if cL.tracer_test:
        wBcIni = 0
        cLcIni = 0

    initialStates = {'thIni': cLthIni,
                     'cumQIni': meas_data_flow['totF'].iloc[0],
                     'cLcIni': cLcIni,
                     'wBvWIni': wBvWIni,
                     'wBcIni': wBcIni
                     }

    iniSt = pd.Series(initialStates)

    # trange_sim = tdseries.trange.view(np.int64)/(1e9 * 3600 * 24)

    # No infiltration (we pass an empty list)
    infilF = []

    emLF = Emission_LF(trange_sim, iniSt, cL, wB, lF, meteo_data,
                       infilF)
    # Call model: this function solves the problem...
    emOut = emLF.SimulateEmissionLF()

    cumqDr, cDr, qDr, qInf, qev, th, m, cInf, mbalerr, vW, mW = emOut

    # Calculate likelihood for simulated data matching measured data
    # We want to fit the data to the weekly outflow data, therefore we take
    # the difference (in the input we selected a 7 day cumulative total)

    outF = meas_data_flow['totF'].iloc[tdseries.tcalmeas_ind].values
    outF = outF - outF[0]
    #like_outF_data = stats.norm(loc=np.diff(outF), scale=stdMeas_outF)

    outF_sim = cumqDr[tdseries.tcalib_ind[tdseries.tmeascal_ind]]
    outF_sim = outF_sim - outF_sim[0]

    if simType == 'ForwardSim':
        iflag = 'sim'
    elif simType == 'ErrEstimation':
        iflag = 'err'
    else:
        iflag = 'est'

    # flow based likelihood for flows over deltaT
    # deltaT = np.diff(
    #     trange_sim[tdseries.tcalib_ind[tdseries.tmeascal_ind]]).mean()
    
    # lowest flow changes with change bF
    likPar_qDr['m_min'] = emLF.BaseFlow(vW[tdseries.tmeascal_ind[1:], -1])
    
    est2, e_est2, std_e2, a2_est, fa2, ExpY2, SimY2, e2 = generalizedLikelihood(
        iflag, likPar_qDr, np.diff(outF_sim)/deltaT, np.diff(outF)/deltaT)

    # Flow based likelihood to estimate cumulative flow
    # stddeviation is based on the cumulatively

    std_errq = likPar_qDr['std0'] + likPar_qDr['std1'] * \
        (ExpY2 - likPar_qDr['m_min'])
    
    std_errF = np.sqrt(np.cumsum(std_errq**2))
    
    # Calculate expected cumulative flow
    ExpCumF = np.cumsum(np.append([0],ExpY2,axis=0))*deltaT

    #compare the total cumulative flows (i.e. last value in time series)
    like_CumF = stats.norm(loc=outF[-1], scale=std_errF[-1])
    est1 = like_CumF.logpdf(ExpCumF[-1])

    cDr_meas = lab_data['Cl'].iloc[tdseries.tcalmeas_lab_ind].values
    #like_cDr_data = stats.norm(loc=cDr_meas, scale=stdMeas_cDr)

    cDr_sim = cDr[tdseries.tcalib_ind[tdseries.tmeascal_lab_ind]]

    # est3, e_est3, std_e3, a3_est, fa3, ExpY3, SimY3, e3 = generalizedLikelihood(
    #     iflag, likPar_cDr, cDr_sim, cDr_meas)

    est3, e_est3, std_e3, a3_est, fa3, ExpY3, SimY3, e3 = generalizedLikelihood(
        iflag, likPar_cDr, np.log10(cDr_sim+1e-12), np.log10(cDr_meas+1e-12))


    if np.isnan(est1):
        est1 = -1e10
    if np.isnan(est2):
        est2 = -1e10
    if np.isnan(est3):
        est3 = -1e10

    if flow_only:
        logp = est1 + est2 # + est3  
    else:
        logp = est1 + est2 + est3  

    if np.isnan(logp):
        logp = -np.inf

    lFPar = {'cL': cL,
             'wB': wB,
             'lF': lF,
             'iniSt': initialStates}

    # like_stats_qDr1 = {'errDist': a1,
    #                    'errDens': fa1,
    #                    'ExpY': ExpY1,
    #                    'SimY': SimY1,
    #                    'logL': est1}
    like_stats_qDr2 = {'errDist': a2_est,
                       'err': e_est2,
                       'std': std_e2,
                       'errDens': fa2,
                       'ExpY': ExpY2,
                       'SimY': SimY2,
                       'logL': est2,
                       'logL_cum': est1,
                       'e': e2}

    like_stats_cDr = {'errDist': a3_est,
                      'err': e_est3,
                      'std': std_e3,
                      'errDens': fa3,
                      'ExpY': ExpY3,
                      'SimY': SimY3,
                      'logL': est3,  # * N3/N1,
                      'e': e3}

    # return logp, emOut, lFPar, emLF, like_stats_qDr1, like_stats_qDr2, like_stats_cDr
    return logp, emOut, lFPar, emLF, like_stats_qDr2, like_stats_cDr, likPar_qDr, likPar_cDr
